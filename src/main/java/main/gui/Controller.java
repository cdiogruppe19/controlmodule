package main.gui;

import de.yadrone.base.navdata.BatteryListener;
import drone_control.DroneNavigationImpl;
import drone_control.DroneSingleton;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import main.gui.interfaces.DroneNavigationInterface;
import main.gui.interfaces.ImageProvider;
import main.util.VideoFileObservable;
import main.util.WebcamController;
import pictureanalysis.supplier.ImageInfo;
import pictureanalysis.supplier.ImageSupplier;

import java.io.*;
import java.net.URL;
import java.text.Collator;
import java.util.*;

public class Controller implements Initializable, Observer {
    @FXML
    ImageView imageView1;
    @FXML
    ChoiceBox<String> imageView1Select;
    @FXML
    ImageView imageView2;
    @FXML
    ChoiceBox<String> imageView2Select;
    @FXML
    ImageView imageView3;
    @FXML
    ChoiceBox<String> imageView3Select;
    @FXML
    ComboBox<String> inputSelection;
    @FXML
    TextArea logTextArea;
    @FXML
    Button clearLogButton;
    @FXML
    Button takeoffButton;
    @FXML
    Button landButton;
    @FXML
    Button autoButton;
    @FXML
    Button idleButton;
    @FXML
    Button centerOnFeatureButton;
    @FXML
    Button approachFeatureButton;
    @FXML
    Button flyThroughFeatureButton;
    @FXML
    Button submitDirectoryButton;
    @FXML
    TextField directoryTextField;
    @FXML
    Button seekSecondButton;
    @FXML
    TextField seekSecondTextField;
    @FXML
    Label totalDurationLabel;
    @FXML
    Button saveConfigButton;
    @FXML
    Button loadConfigButton;
    @FXML
    ProgressBar droneBatteryIndicator;

    private DroneNavigationImpl droneNav = new DroneNavigationImpl();
    private DroneNavigationInterface droneNavigation = droneNav;
    private ImageProvider imageProviderController;
    private ImageSupplier imageSupplier = ImageSupplier.getInstance();
    private GUILog guiLog = GUILog.getInstance();
    private HashMap<ImageView, ChoiceBox<String>> previews = new HashMap<>();
    private boolean choiceBoxesAreSet = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        directoryTextField.setText(System.getProperty("user.dir") + "\\src\\main\\resources\\video\\video1.mp4");
        inputSelection.getItems().addAll("Webcam", "Drone", "Video Feed");
        previews.put(imageView1, imageView1Select);
        previews.put(imageView2, imageView2Select);
        previews.put(imageView3, imageView3Select);
        imageSupplier.addObserver(this);
        guiLog.addObserver(this);
        setDisableVideoFeedButtons(true);
        appendLogText("Initializing...");

        inputSelection.setOnAction(event -> {
            if (imageProviderController != null) this.imageProviderController.stop();
            resetPreviews();
            setDisableVideoFeedButtons(true);
            switch (inputSelection.getValue()){
                case "Webcam":
                    this.imageProviderController = new WebcamController();
                    break;
                case "Drone":
                    this.imageProviderController = droneNav;
                    DroneSingleton.getInstance().getDrone().getNavDataManager().addBatteryListener(new BatteryListener() {
                        public void batteryLevelChanged(int percentage)
                        {
                            //appendLogText("Drone Battery Level Changed: " + percentage);
                            droneBatteryIndicator.setProgress(percentage/100.0);
                        }
                        public void voltageChanged(int vbat_raw) {
                            //appendLogText("Drone Voltage Changed: " + vbat_raw);
                        }
                    });
                    break;
                case "Video Feed":
                    this.imageProviderController = new VideoFileObservable();
                    setDisableVideoFeedButtons(false);
                    break;
                default:
                    break;
            }
            this.imageProviderController.start();
            appendLogText("New input selected: " + inputSelection.getValue());
        });

        takeoffButton.setOnAction(event -> {droneNavigation.takeoff(); appendLogText("Drone taking off...");});
        landButton.setOnAction(event -> {droneNavigation.land(); appendLogText("Landing the drone...");});
        autoButton.setOnAction(event -> {droneNavigation.auto(); appendLogText("Auto mode enabled...");});
        idleButton.setOnAction(event -> {droneNavigation.idle(); appendLogText("Drone set to idle state...");});
        centerOnFeatureButton.setOnAction(event -> {droneNavigation.centerOnFeature(); appendLogText("Centering drone on feature...");});
        approachFeatureButton.setOnAction(event -> {droneNavigation.approachFeature(); appendLogText("Approaching feature...");});
        flyThroughFeatureButton.setOnAction(event -> {droneNavigation.flyThroughFeature(); appendLogText("Flying drone through feature...");});
        clearLogButton.setOnAction(event -> logTextArea.clear());

        submitDirectoryButton.setOnAction(event -> {
            ((VideoFileObservable) imageProviderController).setSeekFrame("0");
            if (!inputSelection.getSelectionModel().isEmpty() && inputSelection.getValue().equals("Video Feed")) {
                imageProviderController.stop();
                ((VideoFileObservable) imageProviderController).setFilePath(directoryTextField.getText());
                resetPreviews();
                imageProviderController.start();
                appendLogText("Directory submitted");
            }
        });

        seekSecondButton.setOnAction(event -> {
            if (inputSelection.getValue().equals("Video Feed") && ((VideoFileObservable) imageProviderController).setSeekFrame(seekSecondTextField.getText())) {
                imageProviderController.stop();
                imageProviderController.start();
            }
        });

        saveConfigButton.setOnAction(event -> {
            List<Integer> previewSelects = new ArrayList<>();
            for (Map.Entry<ImageView, ChoiceBox<String>> entry : previews.entrySet()) {
                previewSelects.add(entry.getValue().getSelectionModel().getSelectedIndex());
            }
            saveVideoFeedConfig(new GUIVideoFeedConfig(
                    directoryTextField.getText(),
                    seekSecondTextField.getText(),
                    ((VideoFileObservable) imageProviderController).getTotalFrames(),
                    previewSelects
            ));
        });

        loadConfigButton.setOnAction(event -> {
            GUIVideoFeedConfig config = loadVideoFeedConfig();
            System.out.println(config);
            inputSelection.getSelectionModel().select(2);
            if (imageProviderController.isRunning()) imageProviderController.stop();
            this.imageProviderController = new VideoFileObservable();
            setDisableVideoFeedButtons(false);
            directoryTextField.setText(config.getFilePath());
            ((VideoFileObservable) imageProviderController).setFilePath(directoryTextField.getText());
            seekSecondTextField.setText(config.getSeekFrame());
            ((VideoFileObservable) imageProviderController).setSeekFrame(config.getSeekFrame(), config.getTotalFrames());
            imageProviderController.start();
            Platform.runLater(() -> {
                try {
                    GUILog.getInstance().add("Loading selected previews...");
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                int i = 0;
                for (Map.Entry<ImageView, ChoiceBox<String>> entry : previews.entrySet()) {
                    entry.getValue().getSelectionModel().select(config.getImageViewSelect().get(i++));
                }
            });
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        List<ImageInfo> images = this.imageSupplier.getImagesWithInfo();

        if (!guiLog.isEmpty()) appendLogText(guiLog.remove(0));

        if (imageSupplier.areImageNamesInitialized() && !choiceBoxesAreSet && !imageSupplier.getImagesWithInfo().isEmpty()) {
            for (Map.Entry<ImageView, ChoiceBox<String>> preview : previews.entrySet()) {
                List<String> options = imageSupplier.getMapKeys();
                options.sort(Collator.getInstance());
                preview.getValue().getItems().setAll(options);
            }
            choiceBoxesAreSet = true;
        }

        for(Map.Entry<ImageView, ChoiceBox<String>> entry : previews.entrySet()){
            for (ImageInfo image : images) {
                if (entry.getValue().getValue() == null) {
                    entry.getKey().setImage(null);
                } else if (entry.getValue().getValue().equals(image.getName())) {
                    entry.getKey().setImage(SwingFXUtils.toFXImage(image.getImage(), null));
                }
            }
            if (inputSelection.getValue().equals("Video Feed")) {
                Platform.runLater(() -> totalDurationLabel.setText(
                        ((VideoFileObservable) imageProviderController).getCurrentFrame() + " of " + ((VideoFileObservable) imageProviderController).getTotalFrames() + " frames"
                ));
            }
        }
    }

    private void appendLogText(String text) {
        logTextArea.appendText(text+"\n");
    }

    private void resetPreviews() {
        for(Map.Entry<ImageView, ChoiceBox<String>> entry : previews.entrySet()){
            entry.getKey().setImage(null);
        }
        for (Map.Entry<ImageView, ChoiceBox<String>> preview : previews.entrySet()) {
            preview.getValue().getSelectionModel().clearSelection();
            preview.getValue().getItems().clear();
        }
        choiceBoxesAreSet = false;
    }

    private void setDisableVideoFeedButtons(boolean disable) {
        seekSecondButton.setDisable(disable);
        seekSecondTextField.setDisable(disable);
        submitDirectoryButton.setDisable(disable);
        directoryTextField.setDisable(disable);
        totalDurationLabel.setVisible(!disable);
        saveConfigButton.setDisable(disable);
    }

    private void saveVideoFeedConfig(GUIVideoFeedConfig config) {
        try {
            FileOutputStream fileOut = new FileOutputStream("src/main/resources/config.cfg");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(config);
            out.close();
            fileOut.close();
            GUILog.getInstance().add("Saved config at \"src/main/resources/config.cfg\"");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            GUILog.getInstance().add("FileNotFoundException...");
        } catch (IOException e) {
            e.printStackTrace();
            GUILog.getInstance().add("IOException...");
        }
    }

    private GUIVideoFeedConfig loadVideoFeedConfig() {
        try {
            FileInputStream fileIn = new FileInputStream("src/main/resources/config.cfg");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            GUIVideoFeedConfig config = (GUIVideoFeedConfig) in.readObject();
            in.close();
            fileIn.close();
            return config;
        } catch (IOException e) {
            e.printStackTrace();
            GUILog.getInstance().add("IOException!");
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            GUILog.getInstance().add("ClassNotFoundException!");
            return null;
        }
    }
}
