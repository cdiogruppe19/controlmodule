package main.gui;

import java.io.Serializable;
import java.util.List;

public class GUIVideoFeedConfig implements Serializable {

    private static final long serialVersionUID = 1000;

    private String seekFrame;
    private int totalFrames;
    private String filePath;
    private List<Integer> imageViewSelect;

    public GUIVideoFeedConfig(String filePath, String seekFrame, int totalFrames, List<Integer> imageViewSelect) {
        this.filePath = filePath;
        this.seekFrame = seekFrame;
        this.totalFrames = totalFrames;
        this. imageViewSelect = imageViewSelect;
    }

    public String getSeekFrame() {
        return seekFrame;
    }

    public void setSeekFrame(String seekFrame) {
        this.seekFrame = seekFrame;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public int getTotalFrames() {
        return totalFrames;
    }

    public void setTotalFrames(int totalFrames) {
        this.totalFrames = totalFrames;
    }

    public List<Integer> getImageViewSelect() {
        return imageViewSelect;
    }

    public void setImageViewSelect(List<Integer> imageViewSelect) {
        this.imageViewSelect = imageViewSelect;
    }

    @Override
    public String toString() {
        return "GUIVideoFeedConfig{" +
                "seekFrame='" + seekFrame + '\'' +
                ", totalFrames='" + totalFrames + '\'' +
                ", filePath='" + filePath + '\'' +
                ", imageViewSelect=" + imageViewSelect +
                '}';
    }
}
