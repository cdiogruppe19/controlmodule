package main.gui.interfaces;

public interface ImageProvider {

    /**
     * Makes the image provider start sending images to the AnalysisController.
     */
    void start();

    /**
     * Makes the image provider stop sending images to the AnalysisController.
     */
    void stop();

    /**
     * Tells if the image provider is currently sending images to the AnalysisController.
     */
    boolean isRunning();
}
