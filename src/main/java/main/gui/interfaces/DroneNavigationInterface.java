package main.gui.interfaces;

public interface DroneNavigationInterface {


    /**
     * Starts the drone and takes off ending in a hover at approximately one meter from the surface.
     */
    void takeoff();

    /**
     * Makes the drone stop what it is doing and lands the drone at it's current position.
     */
    void land();

    /**
     * Makes the drone enter autonomous flight state, searching for the nearest ring and flying through,
     * and continue doing this until other commands are given.
     */
    void auto();

    /**
     * Makes the drone stop what is is doing and hover at it's current position.
     */
    void idle();

    /**
     * Makes the drone center itself in front of the nearest feature if there is one.
     */
    void centerOnFeature();

    /**
     * Makes the drone approach the feature, hovering approximately 50 cm in front of the feature.
     */
    void approachFeature();

    /**
     * Makes the drone fly through a ring - to be used when centered in front of a feature at approximately 50 cm.
     */
    void flyThroughFeature();

    /**
     * Makes the drone center on a ellipse
     */
    void centerOnEllipse();

    /**
     * Sets the speed the drone should fly with, as a percentage of the top speed.
     * @param speed 1-100
     */
    void setSpeed(int speed);
}
