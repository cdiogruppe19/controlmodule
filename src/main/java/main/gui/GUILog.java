package main.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public final class GUILog extends Observable {
    private static volatile GUILog instance = null;
    private List<String> log = new ArrayList<>();

    private GUILog() {}

    public static GUILog getInstance() {
        if (instance == null) {
            synchronized(GUILog.class) {
                if (instance == null) {
                    instance = new GUILog();
                }
            }
        }
        return instance;
    }

    public boolean add(String text) {
        boolean bool = log.add(text);
        getInstance().notifyObservers();
        return bool;
    }

    public List<String> getLog() {
        return log;
    }

    public String remove(int index) {
        return log.remove(index);
    }

    public void clear() {
        log.clear();
    }

    public boolean isEmpty() {
        return log.isEmpty();
    }
}
