package main;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.opencv.core.Core;

import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        URL resource = getClass().getClassLoader().getResource("javafx/application2.fxml");
        assert resource != null;
        Parent root = FXMLLoader.load(resource);
        primaryStage.setTitle("Drone Control");
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("javafx/application.css").toExternalForm());
        System.out.println(scene.getStylesheets());
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
