package main.util;

import main.gui.interfaces.ImageProvider;
import org.jcodec.api.FrameGrab;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import pictureanalysis.controller.AnalysisLibrary;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class VideoFileObservable implements ImageProvider {

    private VideoFileRunnable videoFileRunnable = new VideoFileRunnable();
    private ExecutorService threadExecutor = Executors.newSingleThreadExecutor();
    private Future future;

    @Override
    public void start() {
        if(future == null || future.isDone()) {
            future = threadExecutor.submit(videoFileRunnable);
        }
    }

    @Override
    public void stop() {
        videoFileRunnable.running = false;
        videoFileRunnable.currentFrame = 0;
        videoFileRunnable.totalFrames = 0;
        future.cancel(true);
    }

    @Override
    public boolean isRunning() {
        return videoFileRunnable.running;
    }

    public void setFilePath(String filePath) {
        videoFileRunnable.filePath = filePath;
    }

    public boolean setSeekFrame(String seekFrame) {
        return setSeekFrame(seekFrame, getTotalFrames());
    }

    public boolean setSeekFrame(String seekFrame, int totalFrames) {
        try {
            int temp = Integer.parseInt(seekFrame);
            if (temp < totalFrames && 0 <= temp) {
                videoFileRunnable.seekFrame = temp;
                return true;
            }
            return false;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return false;
        }
    }

    public long getCurrentFrame() {
        return videoFileRunnable.currentFrame;
    }

    public int getTotalFrames() {
        return videoFileRunnable.totalFrames;
    }

    private class VideoFileRunnable implements Runnable {
        private String filePath = null;
        private volatile boolean running = false;
        private int seekFrame = 0;
        private long currentFrame = 0;
        private int totalFrames = 0;

        @Override
        public void run() {
            try {
                if(filePath == null) return;
                File file = new File(filePath);
                FrameGrab grab = FrameGrab.createFrameGrab(NIOUtils.readableChannel(file));
                totalFrames = grab.getVideoTrack().getMeta().getTotalFrames();
                grab.seekToFramePrecise(seekFrame);
                running = true;
                Picture picture;
                while (null != (picture = grab.getNativeFrame()) && running) {
                    AnalysisLibrary.findAllFeatures(AWTUtil.toBufferedImage(picture));
                    currentFrame = grab.getVideoTrack().getCurFrame();
                    // GUILog.getInstance().add(picture.getWidth() + "x" + picture.getHeight() + " " + picture.getColor());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
