package main.util;

import main.gui.interfaces.ImageProvider;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import pictureanalysis.controller.AnalysisLibrary;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class WebcamController implements ImageProvider {
    private VideoCapture capture;
    private Mat frameMat = new Mat();

    public WebcamController(){

    }

    @Override
    public void start() {
        // start the video capture
        this.capture = new VideoCapture(0);
        this.capture.open(0);

        // is the video stream available?
        if (this.capture.isOpened()) {
            // grab a image every 33 ms (30 frames/sec)
            Runnable frameGrabber = this::grabFrame;
            ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
            timer.scheduleAtFixedRate(frameGrabber, 0, 33, TimeUnit.MILLISECONDS);
        } else {
            // log the error
            System.err.println("Failed to open the camera connection...");
        }
    }

    private void grabFrame() {
        if (this.capture.isOpened()) {
            try {
                this.frameMat = new Mat();
                this.capture.read(this.frameMat);
                AnalysisLibrary.findAllFeatures(this.frameMat);
            } catch (Exception e) {
                // log the (full) error
                System.err.print("Exception during the image elaboration...");
                e.printStackTrace();
            }
        }
    }

    @Override
    public void stop() {
        capture.release();
    }

    @Override
    public boolean isRunning() {
        return this.capture.isOpened();
    }
}
