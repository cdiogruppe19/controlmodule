package drone_control;

import de.yadrone.base.IARDrone;

public interface DroneController {

    void setParameters(int speed, int margin_x, int margin_y); //Determines the way the drone flies.
    //The margin is how accurately the drone needs to center itself on a feature to move toward it (in the two dimensions of the image)
    //The speed is how fast the drone flies
    //The method can be ignored, as default values are assigned in the constructor

    void start(); //Start the drone and take off
    void stop(); //Land and shut down the drone
    void search(boolean status); //Should the drone search for features and move towards them?
    IARDrone getDrone(); //Get the drone

}
