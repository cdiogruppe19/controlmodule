package drone_control;

import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;

import java.util.Timer;
import java.util.TimerTask;

public class HoverTimer {

    IARDrone drone;
    CommandManager cm;
    private static HoverTimer timer;

    Timer t;
    TimerTask task;

    private long hoverTimeLimit = 350;

    private boolean stopcalled = false;


    private HoverTimer(){
        drone = DroneSingleton.getInstance().getDrone();
        cm = drone.getCommandManager();
    }

    public static HoverTimer getInstance(){
        if(timer == null){
            timer = new HoverTimer();
        }
        return timer;
    }

    public void stop() {
        stopcalled = true;
    }

    public boolean isStopcalled(){
        return stopcalled;
    }

    public void resetTimer(long ms){
        stopcalled = false;
        try {
            t.cancel();
        } catch (NullPointerException e){
            System.out.println("Timer not initialized, nothing to cancel");
        } finally {
            t = new Timer();
            task = new DCTimerTask();
            t.schedule(task, ms);
        }

    }

    //Inner class used by the timer method
    private class DCTimerTask extends TimerTask {
        @Override
        public void run() {
            if (!stopcalled) {
                //resetTimer(hoverTimeLimit);
                cm.hover();
                System.out.println("HOVERING!");
            }
        }
    }
}
