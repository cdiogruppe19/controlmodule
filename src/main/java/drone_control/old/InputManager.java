package drone_control.old;

import de.yadrone.base.ARDrone;
import drone_control.old.ControlModule;

import java.util.Scanner;

public class InputManager {
    private static ControlModule controlModule;
    private static Scanner scan;

    public static void reset(){
        controlModule = new ControlModule(new ARDrone());
        scan = new Scanner(System.in);
    }

    public static void getInput(){
        reset();
        String input = "";

        while(true){
            input = scan.nextLine();
            try {
                controlModule.movement(input);
            } catch(Exception e){
                System.out.println("OI MATE, DAS NOT A FUNCTION");
            }
        }

    }

}
