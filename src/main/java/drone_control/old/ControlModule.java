package drone_control.old;

import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;

public class ControlModule {
    private IARDrone drone = null;
    private CommandManager commandManager;
    int speed;

    public ControlModule(IARDrone idrone){
        drone = idrone;
        drone.start();
        commandManager = drone.getCommandManager();
        commandManager.resetCommunicationWatchDog();
        speed = 5;
    }

    public void movement(String input) throws Exception{
        switch (input){
            case "t": commandManager.takeOff().doFor(1200);
                commandManager.hover();
                break;
            case "h": commandManager.hover();
                break;
            case "l": commandManager.landing();
                drone.stop();
                System.exit(0);
                break;
            case "w": commandManager.move(10, 10, 0, 0).doFor(1000);
                commandManager.hover();
                break;
            case "a": commandManager.move(0, 10, 0, 0).doFor(1000);
                commandManager.hover();
                break;
            case "s": commandManager.move(-10, 0, 0, 0).doFor(1000);
                commandManager.hover();
                break;
            case "d": commandManager.move(0, -10, 0, 0).doFor(1000);
                commandManager.hover();
                break;
            case "u": commandManager.move(0, 0, -10, 0).doFor(1000);
                commandManager.hover();
                break;
            case "j": commandManager.move(0, 0, 10, 0).doFor(1000);
                commandManager.hover();
                break;
            case "seq": commandManager.move(0, 0, 10, 0).doFor(2000);
                commandManager.hover();
                commandManager.move(10, 0, 0, 0).doFor(2000);
                commandManager.hover();
                commandManager.move(-10, -10, 0, 0).doFor(2000);
                commandManager.hover();
                break;
            default: throw new Exception();
        }
    }
}
