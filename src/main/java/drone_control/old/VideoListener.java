package drone_control.old;

import de.yadrone.base.IARDrone;
import de.yadrone.base.video.ImageListener;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Observable;

public class VideoListener extends Observable implements Runnable {
    private BufferedImage image = null;
    private ByteArrayOutputStream outputStream = null;

    public VideoListener(IARDrone drone) {
        System.out.println("Creating videolistener");
        drone.getVideoManager().addImageListener(new ImageListener() {
            public void imageUpdated(BufferedImage newImage) {
                notifyObservers();
                image = newImage;
            }
        });
    }


    public void run() {
        File file;
        System.out.println("Starting image receiver");
        try {
            int count = 0;
            while(true) {
                Thread.sleep(100);
                if (image != null) {
                    file = new File("Image"+count+".png");
                    count++;
                    System.out.println("Adding image: "+count);
                    try {
                        ImageIO.write(image, "png", this.outputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public ByteArrayOutputStream getOutputStream() {
        return this.outputStream;
    }
}

