package drone_control.states;

import pictureanalysis.features.Feature;

import java.awt.image.BufferedImage;

public class IDLE extends STATE{

    public IDLE(){
        super();
    }

    @Override
    public void adjust(Feature f, BufferedImage image) {
        System.out.println("IDLING");
    }

}
