package drone_control.states;

import drone_control.HoverTimer;
import main.gui.GUILog;
import pictureanalysis.features.Feature;
import pictureanalysis.features.QRCode;

import java.awt.image.BufferedImage;

public class APPROACH extends STATE{

    public APPROACH(){
        super();
    }


    @Override
    public void adjust(Feature f, BufferedImage image) {
        doAdjustment(f, image);
    }

    protected static void doAdjustment(Feature f, BufferedImage image){
        System.out.println("FORWARD!");
        GUILog.getInstance().add("APPROACHING FEATURE");

        if(f.getWidth() < 70){
            cm.forward(speed+7).doFor(600);
        } else if(f.getWidth() < 50){
            cm.forward(speed+5).doFor(450);
        } else {
            //HoverTimer.getInstance().resetTimer(350);
            cm.move(speed+5, 0,0,0).doFor(345);
        }

        //cm.hover();
        //cm.hover().doFor(250);
        cm.hover().doFor(250);

    }
}
