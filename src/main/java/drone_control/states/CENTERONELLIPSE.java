package drone_control.states;

import drone_control.HoverTimer;
import pictureanalysis.controller.AnalysisLibrary;
import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

/**
 * Created by Matt_Lab on 11/06/2018.
 */
public class CENTERONELLIPSE extends STATE {

    int ellipseWidthOld;
    int ellipseWidthNew;
    int count = 0;

    @Override
    public void adjust(Feature f, BufferedImage image) {
        doAdjustment(f, image);
    }

    protected void doAdjustment(Feature f, BufferedImage image) {
        System.out.println("Centering on ellipse");

        if (f instanceof Ellipse && count == 0) {
            ellipseWidthOld = f.getWidth();

            moveLeft();

            count++;

        } else if (f instanceof Ellipse && count > 0) {
            ellipseWidthNew = f.getWidth();
            if (ellipseWidthNew < f.getHeight() + 25 || ellipseWidthNew > f.getHeight() - 25) {
                count = 0;
                ellipseWidthNew = 0;
                ellipseWidthOld = 0;
                CENTER.doAdjustment(f, image);
            } else if (ellipseWidthOld < ellipseWidthNew) {
                moveLeft();
                ellipseWidthOld = ellipseWidthNew;
            } else if (ellipseWidthOld > ellipseWidthNew) {
                moveRight();
                ellipseWidthOld = ellipseWidthNew;
            }
        }

    }

    private void moveLeft() {

        System.out.println("Move left");
        cm.goLeft(15).doFor(300);
        cm.hover().doFor(200);
        cm.spinRight(35).doFor(300);
        cm.hover().doFor(200);

    }

    private void moveRight() {
        System.out.println("Move left");
        cm.goRight(15).doFor(300);
        cm.hover().doFor(200);
        cm.spinLeft(35).doFor(300);
        cm.hover().doFor(200);
    }

}