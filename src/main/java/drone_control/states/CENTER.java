package drone_control.states;

import drone_control.HoverTimer;
import main.gui.GUILog;
import pictureanalysis.features.Feature;

import java.awt.image.BufferedImage;

public class CENTER extends STATE{

    public CENTER(){
        super();
    }

    @Override
    public void adjust(Feature f, BufferedImage image) {
        doAdjustment(f, image);
    }

    protected static void doAdjustment(Feature f, BufferedImage image){
        if(!factorsAreNew){
            calculateFactors(f, image);
        }

        int forward = 0;

        System.out.println("CENTERING!");
        GUILog.getInstance().add("CENTERING WITH FACTORS: " + x_factor + "," + y_factor);
        GUILog.getInstance().add("FEATURE IS: " + f.getWidth() + "PIXELS WIDE");

        if(f.getWidth() < 60){
            forward = speed;
        }

        cm.move(forward, (int)(-1*speed * x_factor), (int)(speed * y_factor), 0).doFor(335);


        //cm.hover();
        //cm.hover().doFor(250);
        cm.hover().doFor(250);

        factorsUsed();
    }
}
