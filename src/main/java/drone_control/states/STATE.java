package drone_control.states;

import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;
import drone_control.DroneSingleton;
import main.gui.GUILog;
import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;
import pictureanalysis.features.QRCode;

import java.awt.image.BufferedImage;

public abstract class STATE {

    protected static IARDrone drone;
    protected static CommandManager cm;
    protected static double x_factor, y_factor, x_margin, y_margin, x_centerval, y_centerval, x_position, y_position, spin_factor;
    protected static int speed = 12;
    protected static boolean rotateRight = true;
    protected static boolean factorsAreNew = false;

    protected static double dif;
    protected static double lastdif = 1.414;

    protected static int numRings = 0;
    protected static int numRingsToGoThrough = 3;

    public STATE(){
        drone = DroneSingleton.getInstance().getDrone();
        cm = drone.getCommandManager();
    }

    public void adjust(Feature f, BufferedImage image){

    }
    public void search(){

    }

    protected static void calculateFactors(Feature f, BufferedImage image){
        x_margin = image.getWidth()/7.1;
        y_margin = image.getHeight()/5.0;

        x_centerval = image.getWidth()/2;
        y_centerval = image.getHeight()/2;

        x_position = f.getPos().x + (f.getWidth() / 2.0);
        if(f instanceof Ellipse){
            y_position = f.getPos().y + (f.getHeight() / 1.5);
        } else {
            y_position = f.getPos().y + (f.getHeight() / 2.0);
        }

        x_factor = direction(x_centerval, x_margin, x_position);
        y_factor = direction(y_centerval, y_margin, y_position);


        if(f instanceof QRCode){
            QRCode code = (QRCode) f;
            if(code.getValue() == null){

                dif = ((double) f.getHeight())/((double) f.getWidth());
                //GUILog.getInstance().add("Difference height/width: " + dif);

                if(dif > lastdif){
                    rotateRight = !rotateRight;
                }

                if(dif < 1.5) {
                    spin_factor = 0;
                } else {
                    if(rotateRight) {
                        spin_factor = -1;
                    } else {
                        spin_factor = 1;
                    }
                }
            }
        } else {
            spin_factor = 0;
        }

        lastdif = dif;
        factorsAreNew = true;
    }

    protected static void factorsUsed(){
        factorsAreNew = false;
    }

    //Used to determine which way the drone should fly to center itself on the feature
    private static int direction(double center, double margin, double position){
        if(position < center - margin){
            return -1;
        }
        else if (position > center + margin){
            return 1;
        }
        else {
            return 0;
        }
    }
}
