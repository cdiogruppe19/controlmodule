package drone_control.states;

import main.gui.GUILog;
import pictureanalysis.features.Feature;

import java.awt.image.BufferedImage;

public class ROTATE extends STATE{

    public ROTATE(){
        super();
    }

    @Override
    public void adjust(Feature f, BufferedImage image) {
        doAdjustment(f, image);
    }

    protected static void doAdjustment(Feature f, BufferedImage image){
        if(!factorsAreNew){
            calculateFactors(f, image);
        }

        GUILog.getInstance().add("ROTATING WITH FACTORS: " + spin_factor);

        cm.move(0, 0, 0, (int)(speed * spin_factor)).doFor(340);
        cm.hover().doFor(50);
        cm.hover().doFor(50);
        cm.hover().doFor(50);
        cm.hover().doFor(50);

        factorsUsed();
    }
}
