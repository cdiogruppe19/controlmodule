package drone_control.states;

import drone_control.HoverTimer;
import main.gui.GUILog;
import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;
import pictureanalysis.features.QRCode;

import java.awt.image.BufferedImage;

public class FLYTHROUGH extends STATE {

    @Override
    public void adjust(Feature f, BufferedImage image) {
        doAdjustment(f, image);
    }

    protected static void doAdjustment(Feature f, BufferedImage image){
        System.out.println("STARTING FLYTHROUGH!");
        GUILog.getInstance().add("STARTING FLYTHROUGH!");

        if(f instanceof QRCode){
            System.out.println("QR code instance...");
            cm.up(40).doFor(1200);
            cm.hover().doFor(300);
            cm.forward(30).doFor(1100);
            cm.hover();
            cm.hover().doFor(300);
            cm.down(20).doFor(500);
            cm.up(25).doFor(300);
        }
        if(f instanceof Ellipse){
            cm.down(20).doFor(250);
        }
        cm.hover().doFor(250);
        numRings++;
    }
}
