package drone_control.states;

import main.gui.GUILog;
import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;
import pictureanalysis.features.QRCode;

import java.awt.image.BufferedImage;

public class AUTO extends STATE{

    private STATE[] states = {new CENTER(), new APPROACH(), new FLYTHROUGH(), new ROTATE()};
    private int currentState;

    private SEARCH[] searches = {new BACK(), new FORW(), new T45(), new SAMEDIR(), new OPPDIR(), new LEFTDIR(), new MASSIVEFOOKINTURN(), new DOWN()};
    private int[] transitions = {1, 2, 5, 0, 0, 2, 7, 0};
    private int currentSearchPattern = 5;

    public void setCurrentSearchPattern(int currentSearchPattern) {
        this.currentSearchPattern = currentSearchPattern;
    }

    @Override
    public void adjust(Feature f, BufferedImage image) {

        boolean flythrough = false;

        if (!factorsAreNew) {
            calculateFactors(f, image);
        }

        System.out.println("AUTOMATIC MOVEMENT:");

        if(f instanceof QRCode){
            QRCode code = (QRCode) f;
            if((f.getWidth() > (image.getWidth() / 2.0) || f.getHeight() > (image.getHeight() / 1.9)) && code.getValue() == null){
                flythrough = true;
            }
            if((f.getWidth() > (image.getWidth() / 3.4) || f.getHeight() > (image.getHeight() / 3.4)) && code.getValue() != null){
                flythrough = true;
            }
        }

        /*
        if(x_factor == 0 && y_factor == 0 && spin_factor != 0) {
            currentState = 1; //1 Approach : Rotate is state 3
        } else*/
        if(x_factor == 0 && y_factor == 0 && flythrough) {
            currentState = 2; //Flythrough
            currentSearchPattern = 0;
        } else if (x_factor == 0 && y_factor == 0) {
            currentState = 1; //Approach
            currentSearchPattern = 0;
        } else {
            currentState = 0; //Center
            currentSearchPattern = 0;
        }

        states[currentState].adjust(f, image);

        factorsUsed();
    }

    @Override
    public void search() {

        GUILog.getInstance().add("Searching:");
        if(numRings >= numRingsToGoThrough){
            currentSearchPattern = 6;
        }
        searches[currentSearchPattern].search();
        currentSearchPattern = transitions[currentSearchPattern];
    }

    private abstract class SEARCH{
        public abstract void search();
    }

    private class BACK extends SEARCH{
        @Override
        public void search() {

            GUILog.getInstance().add("Backwards search");
            cm.backward(100).doFor(250);
            cm.hover().doFor(250);

        }
    }

    private class FORW extends SEARCH{

        @Override
        public void search() {
            GUILog.getInstance().add("Forward search");
            cm.forward(25).doFor(300);
            cm.hover().doFor(250);
        }
    }

    private class T45 extends SEARCH{

        @Override
        public void search() {
            GUILog.getInstance().add("Spinning search");
            cm.move(0,0,0,-40).doFor(1000);
            cm.hover().doFor(250);
        }
    }

    private class SAMEDIR extends SEARCH{

        @Override
        public void search() {
            cm.move(0, (int)(-1*speed * x_factor), 0, 0).doFor(360);
            cm.hover().doFor(250);
        }
    }

    private class OPPDIR extends SEARCH{

        @Override
        public void search() {
            cm.move(0, (int)(speed * x_factor), 0, 0).doFor(360);
            cm.hover().doFor(250);
        }
    }

    private class LEFTDIR extends SEARCH{

        @Override
        public void search() {
            GUILog.getInstance().add("Searching left");

            cm.goLeft(15).doFor(350);
            cm.hover().doFor(250);
        }
    }

    private class MASSIVEFOOKINTURN extends SEARCH {

        @Override
        public void search(){
            numRings = 0;
            cm.forward(30).doFor(700);
            //cm.hover();
            cm.hover().doFor(1000);
            cm.spinRight(100).doFor(700);
            cm.spinLeft(30).doFor(100);
            cm.hover().doFor(600);

        }
    }

    private class DOWN extends SEARCH{

        @Override
        public void search() {
            cm.down(20).doFor(350);
        }
    }

}
