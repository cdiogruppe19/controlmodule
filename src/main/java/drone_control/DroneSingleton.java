package drone_control;

import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;

public class DroneSingleton {

    private static DroneSingleton dsinton;
    private IARDrone drone;

    private DroneSingleton(){
        drone = new ARDrone();
    }

    public static DroneSingleton getInstance() {
        if(dsinton != null){
            return dsinton;
        }
        dsinton = new DroneSingleton();
        return dsinton;
    }

    public IARDrone getDrone(){
        return drone;
    }
}
