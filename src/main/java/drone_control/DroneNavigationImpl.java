package drone_control;

import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;
import de.yadrone.base.video.ImageListener;

import drone_control.states.*;
import main.gui.GUILog;
import main.gui.interfaces.DroneNavigationInterface;
import main.gui.interfaces.ImageProvider;
import pictureanalysis.controller.AnalysisLibrary;
import pictureanalysis.features.Feature;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class DroneNavigationImpl implements DroneNavigationInterface, ImageListener, ImageProvider {

    private IARDrone drone;
    private CommandManager cm;
    private int speed = 10;

    //State machine
    private STATE[] states = { new IDLE(), new AUTO(), new CENTER(), new APPROACH(), new FLYTHROUGH()};
    private int currentState = 0;

    //Image relevant variables
    private BufferedImage latestImage;
    private List<Feature> features = null;

    //Analysis thread
    Thread analysis_thread;             //The thread itself
    private boolean searching = false;  //Boolean to stop the loop in the thread

    //Boolean to stop looking at images
    private boolean sendingImages = false;

    private boolean stopCalled = false;


    public DroneNavigationImpl() {
        //Initialize drone
        drone = DroneSingleton.getInstance().getDrone();
        drone.start();

        cm = drone.getCommandManager();

        //Video settings
        //drone.getCommandManager().setVideoCodec(VideoCodec.H264_720P);
        //drone.getCommandManager().setVideoBitrateControl(VideoBitRateMode.DISABLED);

        //Add the image listener that updates the latest image
        drone.getVideoManager().addImageListener(this);
        //drone.getVideoManager().reinitialize();

    }

    // --- ImageListener methods ---
    @Override
    public void imageUpdated(BufferedImage bufferedImage) {
        if(sendingImages) {
            latestImage = bufferedImage; //Update the image
        }

    }


    // --- DroneNavigation methods ---
    //Get the drone
    public IARDrone getDrone() {
        return drone;
    }

    //Start the drone, take off and hover
    //Allows drone to both hover and adjust
    //Resets hover timer
    @Override
    public void takeoff() {
        //cm.flatTrim();
        //GUILog.getInstance().add("TAAAAAKE OFF MEEEEE");
        ((AUTO) states[1]).setCurrentSearchPattern(5);
        cm.takeOff().doFor(1000);
        cm.hover().doFor(500);
        //HoverTimer.getInstance().resetTimer(600); //Reset the timer until next hover
        stopCalled = false;
    }

    //Lands and stops the drone.
    @Override
    public void land() {
        //Land and stop the drone
        stopCalled = true;
        //HoverTimer.getInstance().stop();
        cm.landing();


    }

    @Override
    public void auto() {
        currentState = 1;
    }

    @Override
    public void idle() {
        currentState = 0;
    }

    @Override
    public void centerOnFeature() {
        currentState = 2;
    }

    @Override
    public void approachFeature() {
        currentState = 3;
    }

    @Override
    public void flyThroughFeature() {
        currentState = 4;
    }

    @Override
    public void centerOnEllipse(){
        currentState = 5;
    }

    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    //Allows latestimage to be set by listener
    //Starts analysis thread
    //Does not change whether drone is allowed to adjust its position
    @Override
    public void start() {
        drone.start();
        sendingImages = true;

        searching = true;
        analysis_thread = new Thread(new AnalysisThread());
        analysis_thread.start(); //Start a new thread with the analysis class. Only 1 thread is active at a time.

    }

    //Does not allow latestimage to be set by listener, effectively stopping all analysis
    //Stops the analysis thread by allowing the while loop to break (setting searching to false)
    @Override
    public void stop() {
        sendingImages = false;
        searching = false;
        drone.stop();
    }

    @Override
    public boolean isRunning() {
        return sendingImages;
    }

    //Analysis thread. Finds features in images and calls the state machine adjust methods
    private class AnalysisThread implements Runnable {

        @Override
        public void run() {

            Feature best = null;
            Feature last = null;
            int bestScore;
            int tempScore;
            int searchcount = 0;

            long startTime;

            //Loop will keep running as long as the drone is searching.
            //Loop is stopped by the stop method (which sets searching = false)
            while (searching) {
                if(latestImage != null) {
                    startTime = System.currentTimeMillis();
                    //GUILog.getInstance().add("Analyzing image");
                    features = AnalysisLibrary.findQr(latestImage);
                    bestScore = 0;

                    for (Feature f : features) {
                        tempScore = f.getScore();

                        //Print all found features
                        //GUILog.getInstance().add("Found feature: " + f.toString() + " - Score: " + tempScore);

                        if (tempScore > bestScore) {
                            best = f;
                            bestScore = tempScore;
                        }
                    }


                    //System.out.println(best);
                    if (best != null) {
                        //Print out the best feature
                        //GUILog.getInstance().add("Best feature: " + best + " - Score: " + bestScore);
                        if(!stopCalled) {
                            states[currentState].adjust(best, latestImage); //Adjustment now a complete sequence instead of sticky command
                            cm.hover().doFor(300);
                            searchcount = 0;
                        }
                    } else if (searchcount > 3 && !stopCalled){
                        //Search if we have had more than 1 analysis without a new feature
                        states[currentState].search();
                        cm.hover().doFor(300);
                        searchcount = 0;
                    } else {
                        searchcount++;
                    }

                    latestImage = null;
                    last = best;
                    best = null;
                    //GUILog.getInstance().add("Analysis took " + (System.currentTimeMillis()-startTime) + " ms");
                } else {
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
    }
}
