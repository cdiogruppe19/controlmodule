package pictureanalysis.controller;

import org.opencv.core.Core;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.features.Feature;
import pictureanalysis.filter.feature.EllipseFilter;
import pictureanalysis.filter.image.GammaCorrectionImageFilter;
import pictureanalysis.filter.image.blob.BlobRemover;
import pictureanalysis.filter.image.mask.*;
import pictureanalysis.filter.image.morph.DialateImageFilter;
import pictureanalysis.filter.image.morph.ErodeImageFilter;
import pictureanalysis.filter.image.morph.OpeningImageFilter;
import pictureanalysis.filter.image.smoothing.GaussianBlurImageFilter;
import pictureanalysis.finder.qr.QRFinder;
import pictureanalysis.finder.qr.QRTextFinder;
import pictureanalysis.finder.ring.ConvexRingFinder;
import pictureanalysis.finder.ring.SingleRingFinder;
import org.opencv.core.Mat;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.utility.ImageConverter;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class AnalysisLibrary {

    static{
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        getQRAnalyzer();
        getRingAnalyzer();
        getRedMessAnalyzer();
        getDarkRingAnalyzer();
        ImageSupplier.getInstance().setInitialized();
    }

    public static List<Feature> findAllFeatures(BufferedImage bufferedImage){
        return findAllFeatures(ImageConverter.bufferedImageToMatRGB(bufferedImage));
    }

    public static List<Feature> findAllFeatures(Mat mat){
        List<Feature> features = new ArrayList<>();

        features.addAll(findQr(mat));
        features.addAll(findRings(mat));

        return features;
    }


    public static List<Feature> findQr(BufferedImage image){
        return findQr(ImageConverter.bufferedImageToMatRGB(image));
    }

    public static List<Feature> findQr(Mat mat){
        List<Feature> features = new ArrayList<>();
        features.addAll(getQRAnalyzer().findFeatures(mat));
        return features;
    }

    public static List<Feature> findRings(BufferedImage image){
        return findRings(ImageConverter.bufferedImageToMatRGB(image));
    }


    public static List<Feature> findRings(Mat mat){
        List<Feature> features = new ArrayList<>();
        //features.addAll(getDarkRingAnalyzer().findFeatures(mat));
        features.addAll(getRingAnalyzer().findFeatures(mat));
        return features;
    }

    public static AnalysisController getRingAnalyzer(){
        return new AnalysisControllerBuilder()
                //.apply(new GammaCorrectionImageFilter(1.6))
                .apply(new GammaCorrectionImageFilter())
                //.saveLastImage("Gamme Corrected")
                .apply(new RedMaskNormalImageFilter())
                .apply(new GaussianBlurImageFilter())
                //.saveLastImage("Red Mask")
                .apply(new BinaryThresholdImageFilter(100))
                //.saveLastImage("Binary Threshold")
                .apply(new SingleRingFinder())
                .apply(new BlobRemover())
                //.saveLastImage("Blobs Removed")
                .apply(new ConvexRingFinder())
                .apply(new EllipseFilter())
                .drawFeaturesOnImages("Circle Features")
                .build();
    }

    public static AnalysisController getDarkRingAnalyzer(){
        return new AnalysisControllerBuilder()
                .apply(new GammaCorrectionImageFilter())
                .saveLastImage("Gamma Corrected")
                .apply(new RedMaskDarkImageFilter())
                .saveLastImage("Dark Mask")
                .apply(new LineFilter())
                .saveLastImage("Line Filter")
                .apply(new BlobRemover())
                .saveLastImage("Blobs Removed Dark")
                .apply(new ConvexRingFinder())
                .apply(new EllipseFilter())
                .drawFeaturesOnImages("Circle Features Dark")
                .build();
    }

    public static AnalysisController getQRAnalyzer(){
        return new AnalysisControllerBuilder()
                .apply(new QRTextFinder())
                .drawFeaturesOnImages("QR Features")
                //.apply(new GammaCorrectionImageFilter(4)))
                //.apply(new WhiteMaskImageFilter())
                .apply(new AdaptiveThresholdingFilter())
                //.saveLastImage("1")
                .apply(new QRFinder())
                .apply(new ErodeImageFilter(2))
                .apply(new DialateImageFilter(2))
                //.saveLastImage("2")
                .apply(new QRFinder())
                .drawFeaturesOnImages("Possible QR Features")
                .build();
    }

    public static AnalysisController getRedMessAnalyzer(){
        return null;
    }




}
