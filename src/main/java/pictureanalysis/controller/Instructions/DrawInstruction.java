package pictureanalysis.controller.Instructions;

import pictureanalysis.controller.AnalysisController;
import pictureanalysis.features.Feature;
import org.opencv.core.Mat;
import pictureanalysis.utility.ImageConverter;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class DrawInstruction extends ImageSupplierInstruction{

    public DrawInstruction(AnalysisController analysisControllerToBuild, String imageName){
        super(analysisControllerToBuild,imageName);
    }
    @Override
    public void runInstruction() {
        Mat editedImage=analysisController.getOriginalImage().clone();
        for(Feature feature: analysisController.getFeatures()){
            editedImage = feature.drawOn(editedImage);
        }
        storeImage(editedImage);

    }
}
