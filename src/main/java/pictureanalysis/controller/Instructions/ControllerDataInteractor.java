package pictureanalysis.controller.Instructions;
import org.opencv.core.*;
import pictureanalysis.features.Feature;

import java.util.List;

public interface ControllerDataInteractor {

    Mat getCurrentImage();
    void setCurrentImage(Mat image);
    void addFeatures(List<Feature> featureList);
}
