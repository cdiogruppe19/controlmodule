package pictureanalysis.controller.Instructions;

import pictureanalysis.controller.AnalysisController;
import pictureanalysis.features.Feature;
import pictureanalysis.finder.Finder;

import java.util.List;

public class FinderInstruction implements Instruction {

    private AnalysisController analysisController;
    private Finder f;

    public FinderInstruction(AnalysisController analysisControllerToBuild, Finder f){
        analysisController = analysisControllerToBuild;
        this.f=f;
    }
    @Override
    public void runInstruction() {
        List<Feature> featureList = f.find(analysisController.getCurrentImage());
        analysisController.addFeatures(featureList);
    }
}
