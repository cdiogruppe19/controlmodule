package pictureanalysis.controller.Instructions;


import pictureanalysis.controller.AnalysisController;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.utility.ImageConverter;


import org.opencv.core.Mat;
import java.awt.image.BufferedImage;
import java.io.IOException;

public abstract class ImageSupplierInstruction implements Instruction{

    protected String imageName;
    protected AnalysisController analysisController;
    protected ImageSupplier supplier = ImageSupplier.getInstance();

    public ImageSupplierInstruction(AnalysisController analysisController, String imageName){
        this.analysisController = analysisController;
        this.imageName =imageName;
    }

    protected void storeImage(Mat image){
        try {
            BufferedImage bufImg;
            bufImg = ImageConverter.matToBufferedImage(image);
            supplier.store(imageName, bufImg,analysisController);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
