package pictureanalysis.controller.Instructions;

public interface Instruction {

    void runInstruction();
}
