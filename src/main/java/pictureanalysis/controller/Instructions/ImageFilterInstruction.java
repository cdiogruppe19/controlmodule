package pictureanalysis.controller.Instructions;

import pictureanalysis.controller.AnalysisController;
import pictureanalysis.filter.image.ImageFilter;

import org.opencv.core.Mat;

public class ImageFilterInstruction implements Instruction {

    private AnalysisController analysisController;
    private ImageFilter f;

    public ImageFilterInstruction(AnalysisController analysisControllerToBuild, ImageFilter f){
        analysisController = analysisControllerToBuild;
        this.f=f;
    }

    @Override
    public void runInstruction() {
        Mat newImage =f.applyFilter(analysisController.getCurrentImage());
        analysisController.setCurrentImage(newImage);
    }
}
