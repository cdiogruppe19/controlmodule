package pictureanalysis.controller.Instructions;

import pictureanalysis.controller.AnalysisController;
import pictureanalysis.features.Feature;
import pictureanalysis.filter.feature.FeatureFilter;

import java.util.List;

public class FeatureFilterInstruction implements Instruction {

    private AnalysisController analysisController;
    private FeatureFilter f;

    public FeatureFilterInstruction(AnalysisController analysisControllerToBuild, FeatureFilter f){
        analysisController = analysisControllerToBuild;
        this.f=f;
    }

    @Override
    public void runInstruction() {
        List<Feature> filteredList =f.applyFilter(analysisController.getFeatures());
        analysisController.setFeatures(filteredList);
    }
}
