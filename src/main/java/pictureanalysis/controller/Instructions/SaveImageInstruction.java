package pictureanalysis.controller.Instructions;

import pictureanalysis.controller.AnalysisController;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.utility.ImageConverter;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class SaveImageInstruction extends ImageSupplierInstruction {

    public SaveImageInstruction(AnalysisController analysisControllerToBuild, String imageName){
        super(analysisControllerToBuild,imageName);
    }

    @Override
    public void runInstruction() {
        storeImage(analysisController.getCurrentImage());

    }
}
