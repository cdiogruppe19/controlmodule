package pictureanalysis.controller;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import pictureanalysis.controller.Instructions.Instruction;
import pictureanalysis.controller.Instructions.ControllerDataInteractor;
import pictureanalysis.features.Feature;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.utility.ImageConverter;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class AnalysisController implements ControllerDataInteractor, FeatureAnalyzer {

    static{System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}


    private List<Instruction> instructions;
    private Mat originalImage;
    private Mat currentImage;

    public List<Feature> getFeatures() {
        return features;
    }

    private List<Feature> features;
    private ImageSupplier imageSupplier;

    private boolean busy=false;

    protected AnalysisController(){
        this(ImageSupplier.getInstance());
    }

    protected AnalysisController(ImageSupplier imageSupplier){
        this.imageSupplier = imageSupplier;
        instructions = new ArrayList<>();
        features= new ArrayList<>();
    }

    void addInstruction(Instruction i){
        instructions.add(i);
    }

    @Override
    public Mat getCurrentImage() {
        return currentImage;
    }

    @Override
    public void setCurrentImage(Mat image) {
        currentImage=image;
    }

    @Override
    public void addFeatures(List<Feature> featureList) {
        features.addAll(featureList);
    }

    @Override
    public List<Feature> findFeatures(BufferedImage img) {
        return findFeatures(ImageConverter.bufferedImageToMatRGB(img));
    }

    @Override
    public List<Feature> findFeatures(Mat mat) {

        synchronized (this) {
            if (busy) {
                throw new ConcurrentModificationException();
            } else {
                busy = true;
            }
        }

        originalImage = mat;
        currentImage = originalImage;
        features = new ArrayList<>();

        try {
            imageSupplier.put("Original", ImageConverter.matToBufferedImage(originalImage));
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < instructions.size(); i++)
            instructions.get(i).runInstruction();

        synchronized (this) {
            busy = false;
            new Thread(() -> imageSupplier.commitStorage(this)).start();
            return features;
        }
    }

    public Mat getOriginalImage() {
        return originalImage;
    }

    public ImageSupplier getImageSupplier() {
        return imageSupplier;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    public boolean isBusy(){
        return busy;
    }
}