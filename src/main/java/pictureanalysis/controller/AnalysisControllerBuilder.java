package pictureanalysis.controller;


import pictureanalysis.controller.Instructions.*;
import pictureanalysis.features.Feature;
import pictureanalysis.filter.feature.FeatureFilter;
import pictureanalysis.filter.image.ImageFilter;
import pictureanalysis.finder.Finder;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.supplier.NameExpert;

public class AnalysisControllerBuilder {

    private AnalysisController controllerToBuild;

    public AnalysisControllerBuilder(ImageSupplier imageSupplier){
        controllerToBuild = new AnalysisController(imageSupplier);
    }

    public AnalysisControllerBuilder(){
        controllerToBuild = new AnalysisController();
    }

    public AnalysisControllerBuilder apply(ImageFilter f){
        controllerToBuild.addInstruction(new ImageFilterInstruction(controllerToBuild,f));
        return this;
    }

    public AnalysisControllerBuilder apply(Finder f){
        controllerToBuild.addInstruction(new FinderInstruction(controllerToBuild,f));
        return this;
    }
    public AnalysisControllerBuilder apply(FeatureFilter f){
        controllerToBuild.addInstruction(new FeatureFilterInstruction(controllerToBuild,f));
        return this;
    }

    public AnalysisControllerBuilder saveLastImage(String imageName){
        NameExpert.putName(imageName);
        controllerToBuild.addInstruction(new SaveImageInstruction(controllerToBuild,imageName));
        return this;
    }

    public AnalysisControllerBuilder drawFeaturesOnImages(String imageName){
        NameExpert.putName(imageName);
        controllerToBuild.addInstruction(new DrawInstruction(controllerToBuild,imageName));
        return this;
    }

    public AnalysisController build(){
        return controllerToBuild;
    }
}
