package pictureanalysis.controller;


import org.opencv.core.Mat;
import pictureanalysis.features.Feature;

import java.awt.image.BufferedImage;
import java.util.List;

public interface FeatureAnalyzer {
    List<Feature> findFeatures(BufferedImage img);
    List<Feature> findFeatures(Mat mat);
}
