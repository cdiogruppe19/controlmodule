package pictureanalysis.features;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;


public class Ellipse extends Feature {

    private double a;
    private double b;
    private Point center;
    private double rotation;


    public Ellipse(Point pos, int width, int height, double a, double b, Point center, double rotation) {
        super(pos, width, height);
        this.a = a;
        this.b = b;
        this.center = center;
        this.rotation = rotation;
    }

    @Override
    public Mat drawOn(Mat image) {
        RotatedRect rotatedRect = new RotatedRect(center, new Size(a, b), rotation);
        Imgproc.ellipse(image, rotatedRect, new Scalar(0, 255, 255), 5);
        return image;
    }

    @Override
    public int getScore(){
        double relative = a/b;
        double errorFactor = 1-Math.abs(relative-1);
        double score = a*b*errorFactor;
        return (int)score;
    }

    @Override
    public String toString() {
        return "Ellipse{" +
                "a=" + a +
                ", b=" + b +
                ", center=" + center +
                ", rotation=" + rotation +
                ", squareArea=" + a*b +
                '}';
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public Point getCenter() {
        return center;
    }

    public double getRotation() {
        return rotation;
    }
}
