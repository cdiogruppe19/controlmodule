package pictureanalysis.features;

import org.opencv.core.Point;

public class QRCode extends Feature {

    String value;

    public QRCode(Point pos, int width, int height, String value) {
        super(pos, width, height);
        this.value = value;
    }

    @Override
    public String toString() {
        return super.toString()+"features.QRCode{" +
                "value='" + value + '\'' +
                '}';
    }

    @Override
    public int getScore(){
        return 100*width*height;
    }

    public String getValue(){
        return value;
    }


}
