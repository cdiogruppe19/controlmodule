package pictureanalysis.features;


import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public abstract class Feature {

    public abstract int getScore();

    //Up left corner
    protected Point pos;

    protected int width;
    protected int height;

    public Feature(Point pos, int width, int height) {
        this.pos = pos;
        this.width = width;
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Point getPos() {
        return pos;
    }

    public void setPos(Point pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "Feature{" +
                "pos=" + pos +
                ", width=" + width +
                ", height=" + height +
                '}';
    }

    public Mat drawOn(Mat image) {
        Imgproc.rectangle(image, pos,new Point(pos.x+width,pos.y+height),new Scalar(0,0,255),3);
        return image;
    }
}
