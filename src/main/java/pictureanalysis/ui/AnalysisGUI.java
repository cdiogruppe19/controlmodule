package pictureanalysis.ui;

import pictureanalysis.supplier.ImageSupplier;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class AnalysisGUI implements Observer{

        JFrame frame;

        JLabel[] images = new JLabel[3];

        ImageSupplier supply;
        String[] pictureNames=null;


    public AnalysisGUI(String[] pictureNames) {
        this();
        this.pictureNames=pictureNames;

    }

    public AnalysisGUI() {
        this.supply=supply;

        frame = new JFrame("Hello Swing");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1920, 1080);
        frame.setVisible(true);
        frame.setLayout(null);

        images[0] = new JLabel();
        images[0].setBounds(0,0,1280,720);
        images[0].setVisible(true);

        images[1] = new JLabel();
        images[1].setBounds(1280,0,640,360);
        images[1].setVisible(true);

        images[2] = new JLabel();
        images[2].setBounds(1280,360,640,360);
        images[2].setVisible(true);

        for (JLabel picLabel:images) {
            frame.add(picLabel);
        }

        supply = ImageSupplier.getInstance();
        supply.addObserver(this);
    }



    public void update() {
        List<BufferedImage> bufferedImages = supply.getImages();
        System.out.println("Updating Frame");
        if(pictureNames==null) {
            for (int i = 0; i < 3; i++) {
                BufferedImage bufImage = bufferedImages.get(i);
                Image image;
                if (i == 0) {
                    image = bufImage.getScaledInstance(1280, 720, Image.SCALE_SMOOTH);
                } else {
                    image = bufImage.getScaledInstance(640, 360, Image.SCALE_SMOOTH);
                }

                images[i].setIcon(new ImageIcon(image));
                images[i].revalidate();
                images[i].repaint();
            }
        }
        else{
            for (int i = 0; i < 3; i++) {
                BufferedImage bufImage=supply.getImage(pictureNames[i]);
                Image image;
                if (i == 0) {
                    image = bufImage.getScaledInstance(1280, 720, Image.SCALE_SMOOTH);
                } else {
                    image = bufImage.getScaledInstance(640, 360, Image.SCALE_SMOOTH);
                }

                images[i].setIcon(new ImageIcon(image));
                images[i].revalidate();
                images[i].repaint();
            }
        }
    }


    @Override
    public void update(Observable o, Object arg) {
        update();

    }
}
