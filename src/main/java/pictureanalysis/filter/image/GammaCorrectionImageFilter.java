package pictureanalysis.filter.image;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class GammaCorrectionImageFilter implements ImageFilter {
    private double gamma;
    private boolean noGammaSet = true;

    public GammaCorrectionImageFilter(){

    }

    public GammaCorrectionImageFilter(double gamma) {
        this.gamma = gamma;
        noGammaSet=false;
    }

    /**
     * Inspired by: https://github.com/tanaka0079/java/blob/master/opencv/image/Gamma.java
     * */
    @Override
    public Mat applyFilter(Mat image) {
        if(noGammaSet){
            gamma=getExpectedGammaValue(image);
        }
        //Create table for gamma transformation
        Mat lut = createGammaTransformationTable();

        //Use table to do gamma correction
        Mat correctedImage = new Mat();
        Core.LUT(image, lut, correctedImage);

        return correctedImage;
    }

    private Mat createGammaTransformationTable(){

        Mat lut = new Mat(1, 256, CvType.CV_8UC1);
        lut.setTo(new Scalar(0));
        for (int i = 0; i < 256; i++) {
            lut.put(0, i, Math.pow((1.0 * i/255), 1/gamma) * 255);
        }
        return lut;
    }

    private static double getExpectedGammaValue(Mat m){
        Mat grayScale = new Mat();
        Imgproc.cvtColor(m,grayScale,Imgproc.COLOR_BGR2GRAY);

        //double sum = 0;
        //double count = grayScale.rows()*grayScale.cols();
        /*for(int row = 0; row<grayScale.rows();row++){
            for(int col=0; col<grayScale.cols(); col++){
                sum+= grayScale.get(row,col)[0];
            }
        }*/

        double average = Core.mean(grayScale).val[0];//sum/count;
        double halfRange= 180;
        double range = 255;
        return Math.max(Math.log(halfRange/range)/Math.log(average/range),1.5);
    }
}
