package pictureanalysis.filter.image;


import org.opencv.core.*;

public interface ImageFilter {

    Mat applyFilter(Mat image);

}
