package pictureanalysis.filter.image.smoothing;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class GaussianBlurImageFilter implements ImageFilter {
    private final int blurSize;

    public GaussianBlurImageFilter() {
        this.blurSize = 11;
    }

    public GaussianBlurImageFilter(int blurSize) {
        this.blurSize = blurSize;
    }

    @Override
    public Mat applyFilter(Mat image) {
        Mat filteredImg = new Mat();
        Imgproc.GaussianBlur(image, filteredImg, new Size(blurSize, blurSize), 0);
        return filteredImg;
    }
}
