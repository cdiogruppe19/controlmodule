package pictureanalysis.filter.image.smoothing;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class BlurImageFilter implements ImageFilter {
    private final int blurSize;

    public BlurImageFilter() {
        this.blurSize = 3;
    }

    public BlurImageFilter(int blurSize) {
        this.blurSize = blurSize;
    }

    @Override
    public Mat applyFilter(Mat image) {
        Mat filteredImg = new Mat();
        Imgproc.blur(image, filteredImg, new Size(blurSize, blurSize));
        return filteredImg;
    }
}
