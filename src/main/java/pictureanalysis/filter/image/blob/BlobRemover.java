package pictureanalysis.filter.image.blob;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

import java.util.ArrayList;
import java.util.List;

import static pictureanalysis.utility.MatOfXConverter.convertIndexesToPoints;

public class BlobRemover implements ImageFilter
{
    //https://docs.opencv.org/2.4/modules/imgproc/doc/structural_analysis_and_shape_descriptors.html
    @Override
    public Mat applyFilter(Mat image) {

        List<MatOfPoint> contours= new ArrayList<>();
        Mat hierarchy = new Mat();
        Mat imageCopy;
        imageCopy=image.clone();

        Imgproc.findContours(imageCopy, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        Mat newImage = getCleanImage(image.rows(),image.cols(),image.type());

        for(int i =0; i<contours.size();i++) {
            MatOfPoint contour = contours.get(i);
            MatOfInt hull = new MatOfInt();
            Imgproc.convexHull(contour,hull);

            MatOfPoint2f hullContour = new MatOfPoint2f(convertIndexesToPoints(contour,hull).toArray());

            double hullArea = Imgproc.contourArea(hullContour);
            double contourArea = Imgproc.contourArea(contour);

            if(checkEllipseRatio(hullArea, contourArea)){
                contours.remove(i);
                i--;
                continue;
            }

            if(hullContour.toArray().length<10){
                contours.remove(i);
                i--;
                continue;
            }

            Imgproc.drawContours(newImage,contours,i,new Scalar(255),-1);
        }

        return newImage;
    }


    private boolean checkEllipseRatio(double hullArea,double contourArea){
        return hullArea<300||hullArea/contourArea<5;
    }

    private Mat getCleanImage(int rows, int cols, int type){
        Mat newImage = new Mat(rows,cols,type);
        Imgproc.threshold(newImage,newImage,256,255,Imgproc.THRESH_BINARY);

        return newImage;
    }

}
