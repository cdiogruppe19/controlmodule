package pictureanalysis.filter.image.morph;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class DialateImageFilter extends MorphImageFilter implements ImageFilter {

    public DialateImageFilter(){

    }
    public DialateImageFilter(int structureSize){
        super(structureSize);
    }
    @Override
    public Mat applyFilter(Mat image) {
        setupStructureElement();
        Mat dilatedImage = new Mat();
        Imgproc.dilate(image,dilatedImage, structureElement);

        return dilatedImage;
    }
}
