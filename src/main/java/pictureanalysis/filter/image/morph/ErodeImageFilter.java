package pictureanalysis.filter.image.morph;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class ErodeImageFilter extends MorphImageFilter implements ImageFilter {
    public ErodeImageFilter(){ }

    public ErodeImageFilter(int structureSize){
        super(structureSize);
    }

    @Override
    public Mat applyFilter(Mat image) {
        setupStructureElement();
        Mat erodeImage = new Mat();
        Imgproc.erode(image,erodeImage, structureElement);

        return erodeImage;
    }

}
