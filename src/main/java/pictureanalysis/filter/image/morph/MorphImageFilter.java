package pictureanalysis.filter.image.morph;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public abstract class MorphImageFilter implements ImageFilter {

    protected int structureSize= 15;
    protected Mat structureElement;
    public MorphImageFilter(){

    }
    public MorphImageFilter(int structureSize){
        this.structureSize=structureSize;
    }

    protected void setupStructureElement(){
        int sizeK = 2*structureSize+1;
        structureElement = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE,new Size(sizeK,sizeK));
    }

}
