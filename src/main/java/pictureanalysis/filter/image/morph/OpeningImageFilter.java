package pictureanalysis.filter.image.morph;

import org.opencv.core.Mat;
import pictureanalysis.filter.image.ImageFilter;

public class OpeningImageFilter implements ImageFilter {
    DialateImageFilter dialate;
    ErodeImageFilter erode;

    public OpeningImageFilter(){
        dialate =new DialateImageFilter(10);
        erode =new ErodeImageFilter(10);
    }
    public OpeningImageFilter(int dialSize, int erodeSize){
        dialate =new DialateImageFilter(dialSize);
        erode =new ErodeImageFilter(erodeSize);
    }

    @Override
    public Mat applyFilter(Mat image) {
        Mat dilatedImage =dialate.applyFilter(image);
        Mat erodedImage =erode.applyFilter(dilatedImage);
        return erodedImage;
    }
}
