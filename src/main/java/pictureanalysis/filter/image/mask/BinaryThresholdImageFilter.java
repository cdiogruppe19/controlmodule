package pictureanalysis.filter.image.mask;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class BinaryThresholdImageFilter implements ImageFilter {
    int threshold;
    public BinaryThresholdImageFilter(int threshold){
        this.threshold=threshold;
    }
    @Override
    public Mat applyFilter(Mat image) {
        Mat outImage = new Mat();
        Imgproc.threshold(image,outImage,threshold,255,Imgproc.THRESH_BINARY);

        return outImage;
    }
}
