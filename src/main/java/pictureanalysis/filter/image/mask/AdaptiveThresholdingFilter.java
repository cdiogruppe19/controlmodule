package pictureanalysis.filter.image.mask;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class AdaptiveThresholdingFilter implements ImageFilter{

    @Override
    public Mat applyFilter(Mat image) {
        Mat adaptedImage = new Mat();
        Mat grayScale = new Mat();
        Imgproc.cvtColor(image,grayScale,Imgproc.COLOR_BGR2GRAY);
        Imgproc.adaptiveThreshold(grayScale,adaptedImage,255.0,Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,Imgproc.THRESH_BINARY,11, 2);

        return adaptedImage;
    }
}
