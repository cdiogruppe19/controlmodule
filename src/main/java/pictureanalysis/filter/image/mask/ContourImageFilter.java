package pictureanalysis.filter.image.mask;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

import java.util.ArrayList;
import java.util.List;

public class ContourImageFilter implements ImageFilter {
    private Scalar color;
    private int thickness;

    public ContourImageFilter(Scalar color, int thickness) {
        this.color = color;
        this.thickness = thickness;
    }

    @Override
    public Mat applyFilter(Mat image) {
        Mat newImage = new Mat(image.rows(), image.cols(), image.type());
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        for (int i = 0; i < contours.size(); i++)
            Imgproc.drawContours(newImage, contours, i, color, thickness);
        return newImage;
    }
}
