package pictureanalysis.filter.image.mask;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class LineFilter implements ImageFilter {
    @Override
    public Mat applyFilter(Mat image) {
        Mat lines = new Mat();

        double minLineLength= 100;
        double maxLineGap =100;
        Imgproc.HoughLinesP(image, lines,1.0, Math.PI/180,200,minLineLength,maxLineGap);

        Mat newImage = new Mat(image.rows(),image.cols(),image.type());

        for(int i=0;i<lines.rows(); i++){
            double[] line = lines.get(i,0);
            Point point1 = new Point(line[0],line[1]);
            Point point2 = new Point(line[2],line[3]);

            Imgproc.line(newImage, point1, point2, new Scalar(255),5);
        }
        Core.bitwise_not(newImage,newImage);
        Core.bitwise_and(newImage,image,image);

        return image;
    }
}
