package pictureanalysis.filter.image.mask;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class RedMaskNormalImageFilter implements ImageFilter {
    @Override
    public Mat applyFilter(Mat image) {
        Mat hsv = new Mat();
        Imgproc.cvtColor(image,hsv,Imgproc.COLOR_BGR2HSV);

        Mat orangeRedMask = new Mat(), pinkRedMask = new Mat();
        Mat redMask = new Mat();

        Core.inRange(hsv, new Scalar(0, 70, 50), new Scalar(10, 255, 255), orangeRedMask);
        Core.inRange(hsv, new Scalar(150, 50, 70), new Scalar(180, 255, 255), pinkRedMask);
        Core.bitwise_or(orangeRedMask, pinkRedMask, redMask);

        return redMask;
    }
}
