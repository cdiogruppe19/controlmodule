package pictureanalysis.filter.image.mask;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class RedMaskDarkImageFilter implements ImageFilter {
    @Override
    public Mat applyFilter(Mat image) {
        Mat hsv = new Mat();
        Imgproc.cvtColor(image,hsv,Imgproc.COLOR_BGR2HSV);

        Mat brownRedMask = new Mat(), purpleRedMask = new Mat();
        Mat redMask = new Mat();

        Core.inRange(hsv, new Scalar(0, 150, 60), new Scalar(17, 255, 150), brownRedMask);
        Core.inRange(hsv, new Scalar(110, 40, 60), new Scalar(180, 210, 210), purpleRedMask);
        Core.bitwise_or(brownRedMask, purpleRedMask, redMask);

        return redMask;
    }
}
