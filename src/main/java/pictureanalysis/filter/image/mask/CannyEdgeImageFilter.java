package pictureanalysis.filter.image.mask;

import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class CannyEdgeImageFilter implements ImageFilter {

    private final double minThreshold, maxThreshold;
    private final int apertureSize;

    public CannyEdgeImageFilter(double minThreshold, double maxThreshold) {
        this.minThreshold = minThreshold;
        this.maxThreshold = maxThreshold;
        this.apertureSize = 3;
    }

    public CannyEdgeImageFilter(double minThreshold, double maxThreshold, int apertureSize) {
        this.minThreshold = minThreshold;
        this.maxThreshold = maxThreshold;
        this.apertureSize = apertureSize;
    }

    @Override
    public Mat applyFilter(Mat image) {
        Mat detectedEdges = new Mat();
        Mat grayImage = getGrayScaleImage(image);

        Imgproc.Canny(grayImage, detectedEdges, minThreshold, maxThreshold, apertureSize, false);
        return detectedEdges;
    }

    private Mat getGrayScaleImage(Mat image){
        Mat grayImage = new Mat();

        if (image.channels() > 1)
            Imgproc.cvtColor(image, grayImage, Imgproc.COLOR_BGR2GRAY);
        else
            grayImage = image;

        return grayImage;
    }
}
