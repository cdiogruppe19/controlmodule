package pictureanalysis.filter.image.mask;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.filter.image.ImageFilter;

public class WhiteMaskImageFilter implements ImageFilter {
    @Override
    public Mat applyFilter(Mat image) {
        Mat hsv = new Mat();
        Imgproc.cvtColor(image,hsv,Imgproc.COLOR_BGR2HSV);

        Mat whiteMask = new Mat();
        Core.inRange(hsv, new Scalar(0, 0, 230), new Scalar(255, 30, 255), whiteMask);

        return whiteMask;
    }
}
