package pictureanalysis.filter.feature;

import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * This filter removes ellipses that are highly unlikely to be real ellipse.
 * This is based on their rotation and their size. Usually this means small ellipses,
 * and big angled ellipses.
 */
public class EllipseFilter implements FeatureFilter{
    @Override
    public List<Feature> applyFilter(List<Feature> features) {
        List<Ellipse> ellipseList = new ArrayList<>();

        features.stream().filter(feature->feature instanceof Ellipse)
                .forEach(feature->ellipseList.add((Ellipse) feature));

        List<Ellipse> removedEllipseList =ellipseList.stream()
                .filter(ellipse -> !validateEllipse(ellipse)).collect(Collectors.toList());

        features=features.stream()
                .filter(feature -> !removedEllipseList.contains(feature))
                .collect(Collectors.toList());

        return features;
    }

    private boolean validateEllipse(Ellipse ellipse){
        double minimumArea = 20000;

        double ellipseArea =ellipse.getA()*ellipse.getB();
        if(ellipseArea<minimumArea){
            return false;
        }

        if(ellipse.getB()/ellipse.getA()<1.2)
            return true;

        //Unit Degree
        double rotationAngle = ellipse.getRotation();
        double allowedSlope = 30;

        if(rotationAngle<=allowedSlope||rotationAngle>360-allowedSlope){
            return true;
        }
        else if(rotationAngle<=180+allowedSlope&&rotationAngle>=180-allowedSlope){
            return true;
        }
        return false;
    }
}
