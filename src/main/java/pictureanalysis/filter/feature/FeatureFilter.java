package pictureanalysis.filter.feature;

import pictureanalysis.features.Feature;

import java.util.List;

public interface FeatureFilter {
    List<Feature> applyFilter(List<Feature> features);
}
