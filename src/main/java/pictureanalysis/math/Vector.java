package pictureanalysis.math;

import org.opencv.core.Point;

public class Vector {
    public double x, y;

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector(Point p1, Point p2) {
        this.x = p2.x - p1.x;
        this.y = p2.y - p1.y;
    }

    public double length() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public double dot(Vector v) {
        return this.x * v.x + this.y * v.y;
    }

    public double angle(Vector v) {
        return Math.acos(this.dot(v) / (this.length() * v.length()));
    }
}
