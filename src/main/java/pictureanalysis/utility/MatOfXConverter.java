package pictureanalysis.utility;

import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;

import java.util.List;

public class MatOfXConverter {

    public static MatOfPoint convertIndexesToPoints(MatOfPoint contour, MatOfInt indexes) {
        int[] arrIndex = indexes.toArray();
        Point[] arrContour = contour.toArray();
        return convertPointArrayToMatOfPoints(arrContour,arrIndex);
    }
    public static MatOfPoint convertPointListToMatOfPoints(List<Point> points) {
        int[] arrIndex = new int[points.size()];
        for(int i = 0; i<arrIndex.length;i++)
            arrIndex[i]=i;
        Point[] arrContour = new Point[points.size()];
        for(int i = 0; i< arrContour.length;i++)
            arrContour[i]= points.get(i);
        return convertPointArrayToMatOfPoints(arrContour,arrIndex);
    }

    private static MatOfPoint convertPointArrayToMatOfPoints(Point[] arrContour,int[] arrIndex){
        Point[] arrPoints = new Point[arrIndex.length];

        for (int i=0;i<arrIndex.length;i++) {

            arrPoints[i] = arrContour[arrIndex[i]];
            //System.out.println(arrPoints[i].x+" "+arrPoints[i].y);
        }

        MatOfPoint hull = new MatOfPoint();
        hull.fromArray(arrPoints);
        return hull;
    }
}
