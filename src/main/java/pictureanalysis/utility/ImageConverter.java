package pictureanalysis.utility;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;


public class ImageConverter {

    public static Mat bufferedImageToMatRGB(BufferedImage bi){
        Mat mat = new Mat(bi.getHeight(),bi.getWidth(), CvType.CV_8UC3);
        byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
        mat.put(0,0,data);
        return mat;
    }

    public static Mat bufferedImageToMatBW(BufferedImage bi){
        Mat mat = new Mat(bi.getHeight(),bi.getWidth(), CvType.CV_8UC1);
        byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
        mat.put(0,0,data);
        return mat;
    }

    public static BufferedImage matToBufferedImage(Mat m) throws IOException {
        MatOfByte mob = new MatOfByte();
        Imgcodecs.imencode(".bmp",m,mob);
        return ImageIO.read(new ByteArrayInputStream(mob.toArray()));
    }
}
