package pictureanalysis.finder.qr;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.features.Feature;
import pictureanalysis.features.QRCode;
import pictureanalysis.finder.Finder;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.utility.ImageConverter;

import java.io.IOException;
import java.util.*;

public class QRFinder implements Finder {
    public boolean drawContours = false;

    /**
     * Inspired by: https://github.com/opencv/opencv/blob/master/samples/cpp/squares.cpp
     */
    @Override
    public List<Feature> find(Mat image) {
        List<Feature> qrList = new ArrayList<>();
        List<Rect> rectangles = new ArrayList<>();

        // find contours
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_NONE);

        if (drawContours) {
            Mat mask = Mat.zeros(image.rows(), image.cols(), CvType.CV_8UC1);
            Imgproc.drawContours(mask, contours, -1, new Scalar(255), 0);
            try {
                ImageSupplier.getInstance().put("contours", ImageConverter.matToBufferedImage(mask));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (MatOfPoint contour : contours) {

            Point[] contourArray = contour.toArray();
            MatOfPoint2f contour2f = new MatOfPoint2f(contourArray);

            // if contour is not reasonably sized, skip
            if (contourArray.length > image.width() * 2 + image.height() * 2) {
                continue;
            }


            // approximate contour with accuracy proportional
            // to the contour perimeter
            MatOfPoint2f approx = new MatOfPoint2f();
            Imgproc.approxPolyDP(contour2f, approx, Imgproc.arcLength(contour2f, true) * 0.02, true);
            Point[] approxArray = approx.toArray();

            // square contours should have 4 vertices after approximation
            // relatively large area (to filter out noisy contours)
            // and be convex.
            // Note: absolute value of an area is used because
            // area may be positive or negative - in accordance with the
            // contour orientation


            //TODO: Gør det nogen forskel at tjekke om contour er convex. Udelader det gode firkanter?
            //TODO: polygoner med flere end 4 punkter bør tjekkes. Især hvis der er tale om bøjede kurver?
            if (approxArray.length >= 4
                && Math.abs(Imgproc.contourArea(approx.t())) > 400
                && Imgproc.isContourConvex(new MatOfPoint(approxArray)))
            {

                double maxCosine = 0;
                for (int j = 2; j < 5; j++) {
                    // find the maximum cosine of the angle between joint edges
                    double cosine = Math.abs(angle(approxArray[j % 4], approxArray[j - 2], approxArray[j - 1]));
                    maxCosine = Math.max(maxCosine, cosine);
                }

                // if cosines of all angles are small
                // (all angles are ~90 degree) then write quandrange
                // vertices to resultant sequence
                if (maxCosine >= 0.3) continue;

                Rect rect = Imgproc.boundingRect(new MatOfPoint(approxArray));
                rectangles.add(rect);

            }
        }


        // sort rectangles by area in descending order
        rectangles.sort(Comparator.comparingDouble(Rect::area));
        Collections.reverse(rectangles);

        // search for recursive rectangles
        List<RecursiveRect> recursiveRects = new ArrayList<>();

        outer:
        for (Rect r : rectangles) {
            // skip rectangles already found as children
            for (RecursiveRect recursiveRect : recursiveRects)
                if (recursiveRect.children.contains(new RecursiveRect(r)))
                    continue outer;

            // save recursive rectangles
            RecursiveRect rect = findRecursiveRect(r, rectangles);
            if (rect.children != null) {
                recursiveRects.add(rect);
            }
        }


        // if rectangle matches paper/border
        if (recursiveRects.size() == 1) {
            RecursiveRect r = recursiveRects.get(0);
            //if (!r.isSquare())
                qrList.add(r.toQRCode());
        }
        // if rectangles matches QR squares
        else if (recursiveRects.size() > 1) {
            boolean allRectsAreSquares = true;
            for (RecursiveRect r : recursiveRects)
                if (!r.isSquare()) allRectsAreSquares = false;

            if (allRectsAreSquares && recursiveRects.get(0).isDistanceMatching(recursiveRects.get(1))) {
                qrList.add(squaresToQRCode(recursiveRects));
            }
        }

        return qrList;
    }

    private Feature squaresToQRCode(List<RecursiveRect> recursiveRects) {
        int xMin = recursiveRects.get(0).x;
        int xMax = recursiveRects.get(0).x;
        int yMin = recursiveRects.get(0).y;
        int yMax = recursiveRects.get(0).y;

        for (RecursiveRect r : recursiveRects) {
            if (r.x < xMin) xMin = r.x;
            if (r.x + r.width > xMax) xMax = r.x + r.width;
            if (r.y < yMin) yMin = r.y;
            if (r.y + r.height > yMax) yMax = r.y + r.height;
        }

        if (recursiveRects.size() == 2) {
            // if two vertically aligned squares
            if (xMax-xMin < yMax-yMin) {
                xMax = xMin + (yMax-yMin);
            }
            // if two horizontally aligned squares
            else {
                yMax = yMin + (xMax-xMin);
            }
        }

        return new QRCode(new Point(xMin, yMin), xMax-xMin, yMax-yMin, null);
    }

    private class RecursiveRect extends Rect {
        List<RecursiveRect> children;

        RecursiveRect(Rect obj, List<RecursiveRect> children) {
            super(obj.x, obj.y, obj.width, obj.height);
            this.children = children;
        }

        RecursiveRect(Rect obj) {
            super(obj.x, obj.y, obj.width, obj.height);
            this.children = null;
        }

        @Override
        public String toString() {
            return "RecursiveRect{" +
                    "obj=" + super.toString() +
                    ", children=" + children +
                    '}';
        }

        boolean isSquare() {
            return (double) width / height < 1.1 && (double) width / height > 0.9;
        }

        QRCode toQRCode() {
            return new QRCode(tl(), width, height, null);
        }

        // returns true if distance between rects is about the size of the width/height
        boolean isDistanceMatching(RecursiveRect r) {
            Rect a = this, b = r;
            // make sure that a is the left rect
            if (this.x > r.x) {
                a = r;
                b = this;
            }

            // calculate distances
            int distanceX = a.x + a.width - b.x;
            int distanceY = a.y + a.height - b.y;

            // check if distances match width and height of rect
            boolean xIsWidthClose = distanceX > (a.width * 0.6) && distanceX < (a.width * 1.1);
            boolean yIsHeightClose = distanceY > (a.height * 0.6) && distanceY < (a.height * 1.1);

            return xIsWidthClose || yIsHeightClose;
        }
    }

    private RecursiveRect findRecursiveRect(Rect outer, List<Rect> rects) {
        List<RecursiveRect> children = new ArrayList<>();

        // check all rectangles
        for (Rect inner : rects) {
            // if rect is outer, skip
            if (outer == inner) continue;
            // if rect is inside outer, add rect to children as a recursive rect
            if (isRectInsideRect(outer, inner)) {
                RecursiveRect innerRecursive = findRecursiveRect(inner, rects);
                children.add(innerRecursive);
            }
        }

        // if children
        if (children.size() > 0)
            return new RecursiveRect(outer, children);
            // if no children
        else
            return new RecursiveRect(outer, null);
    }

    private boolean isRectInsideRect(Rect outer, Rect inner) {
        int minX = outer.x;
        int maxX = getUpperX(outer);
        int minY = outer.y;
        int maxY = getUpperY(outer);

        return inner.x >= minX && inner.y >= minY && getUpperX(inner) <= maxX && getUpperY(inner) <= maxY;
    }

    private static int getUpperX(Rect rect) {
        return rect.x + rect.width;
    }

    private static int getUpperY(Rect rect) {
        return rect.y + rect.height;
    }

    // helper function:
    // finds a cosine of angle between vectors
    // from pt0->pt1 and from pt0->pt2
    private static double angle(Point pt1, Point pt2, Point pt0) {
        double dx1 = pt1.x - pt0.x;
        double dy1 = pt1.y - pt0.y;
        double dx2 = pt2.x - pt0.x;
        double dy2 = pt2.y - pt0.y;
        return (dx1 * dx2 + dy1 * dy2) / Math.sqrt((dx1 * dx1 + dy1 * dy1) * (dx2 * dx2 + dy2 * dy2) + 1e-10);
    }

}
