package pictureanalysis.finder.ring;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;
import pictureanalysis.finder.Finder;

import java.util.ArrayList;
import java.util.List;

public class HoughCircleFinder implements Finder {

    /**
     * Inspired by: https://docs.opencv.org/3.3.1/d4/d70/tutorial_hough_circle.html
     */
    @Override
    public List<Feature> find(Mat image) {
        List<Feature> features = new ArrayList<>();

        Mat gray = new Mat();
        if (image.type() == CvType.CV_8UC3) {
            Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        } else {
            gray = image;
        }

        Imgproc.medianBlur(gray, gray, 5);
        Mat circles = new Mat();

        double param2 = 20;
        int i = 0;
        hough(gray, circles, param2);

        while (circles.cols() > 1 && i < 10) {
            param2 += 5;
            hough(gray, circles, param2);
            i++;
        }

        for (int x = 0; x < circles.cols(); x++) {
            double[] c = circles.get(0, x);
            Point center = new Point(Math.round(c[0]), Math.round(c[1]));
            int radius = (int) Math.round(c[2]);
            features.add(new Ellipse(center, radius * 2, radius * 2, radius * 2, radius * 2, center, 0));
        }

        return features;
    }

    private void hough(Mat gray, Mat circles, double param2){
        Imgproc.HoughCircles(gray, circles, Imgproc.HOUGH_GRADIENT,
                1.0,                          // the inverse ratio of resolution.
                100, // minimum distance between detected centers.
                50.0,                      // upper threshold for the internal Canny edge detector.
                param2,                           // threshold for center detection.
                0,                       // minimum radius to be detected. If unknown, put zero as default.
                0);                      // maximum radius to be detected. If unknown, put zero as default.
    }

}
