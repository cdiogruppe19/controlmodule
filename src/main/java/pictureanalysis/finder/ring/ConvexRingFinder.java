package pictureanalysis.finder.ring;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;
import pictureanalysis.finder.Finder;
import pictureanalysis.utility.MatOfXConverter;

import java.util.ArrayList;
import java.util.List;

import static pictureanalysis.utility.MatOfXConverter.convertIndexesToPoints;

public class ConvexRingFinder implements Finder{
    @Override
    public List<Feature> find(Mat image) {

        List<MatOfPoint> contours= new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        List<Point> points = new ArrayList<>();
        for(int i = 0; i< contours.size();i++) {
            MatOfPoint contour = contours.get(i);
            points.addAll(contour.toList());
        }
        MatOfInt hull = new MatOfInt();

        MatOfPoint contourPoints = MatOfXConverter.convertPointListToMatOfPoints(points);

        if(contourPoints.toList().size()<1){
            return new ArrayList<>();
        }

        Imgproc.convexHull(contourPoints,hull);


        MatOfPoint2f hullContour = new MatOfPoint2f(convertIndexesToPoints(contourPoints,hull).toArray());
        MatOfPoint circleContour= new MatOfPoint();
        hullContour.convertTo(circleContour,CvType.CV_32S);


        List<Feature> rings = new ArrayList<>();
        if(hullContour.toList().size()<5){
            return new ArrayList<>();
        }

        RotatedRect fittedEllipse = Imgproc.fitEllipse(hullContour);
        Rect boundingBox =Imgproc.boundingRect(circleContour);
        rings.add(new Ellipse(new Point(boundingBox.x, boundingBox.y)
                        , boundingBox.width
                        , boundingBox.height
                        , fittedEllipse.size.width
                        , fittedEllipse.size.height
                        , new Point((int) fittedEllipse.center.x, (int) fittedEllipse.center.y)
                        , fittedEllipse.angle
                )
        );


        return rings;
    }
}
