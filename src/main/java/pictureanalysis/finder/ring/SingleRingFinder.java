package pictureanalysis.finder.ring;

import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import pictureanalysis.features.Ellipse;
import pictureanalysis.features.Feature;
import pictureanalysis.finder.Finder;

import java.util.ArrayList;
import java.util.List;

public class SingleRingFinder implements Finder {

    static{System.loadLibrary(Core.NATIVE_LIBRARY_NAME);}

    @Override
    public List<Feature> find(Mat image) {
        List<Feature> rings = new ArrayList<>();

        //Get ready to find contours.
        List<MatOfPoint> list = new ArrayList<>();
        Mat hierarchy = new Mat();

        Imgproc.findContours(image, list, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);

        for (int i = 0; i < list.size(); i++) {
            MatOfPoint contourPoints = list.get(i);

            //Check if contour is reasonably sized.
            if (contourPoints.toArray().length < 7 || contourPoints.toArray().length <= image.width() / 8 || contourPoints.toArray().length > image.width() * 2 + image.height() * 2) {
                list.remove(i);
                i--;
                continue;
            }

            //Fit an ellipse to contour
            MatOfPoint2f contourPoints2f = new MatOfPoint2f(contourPoints.toArray());
            RotatedRect fittedEllipse = Imgproc.fitEllipse(contourPoints2f);

            Mat drawnEllipseImage = new Mat(image.rows(), image.cols(), image.type());
            Mat drawContourImage = new Mat(image.rows(), image.cols(), image.type());

            Imgproc.ellipse(drawnEllipseImage, fittedEllipse, new Scalar(255), 15);
            Imgproc.drawContours(drawContourImage, list, i, new Scalar(255), 15);

            //Check diference between 2 drawings.
            Mat contourEllipseDif = new Mat(image.rows(), image.cols(), image.type());
            Core.absdiff(drawnEllipseImage, drawContourImage, contourEllipseDif);
            int numWrongPixels = Core.countNonZero(contourEllipseDif);
            int contourArea = Core.countNonZero(drawContourImage);


            if ((double) numWrongPixels / contourArea > 0.4) {
                list.remove(i);
                i--;
                continue;
            }

            Rect boundingBox = fittedEllipse.boundingRect();

            rings.add(new Ellipse(new Point(boundingBox.x, boundingBox.y)
                            , boundingBox.width
                            , boundingBox.height
                            , fittedEllipse.size.width
                            , fittedEllipse.size.height
                            , new Point((int) fittedEllipse.center.x, (int) fittedEllipse.center.y)
                            , fittedEllipse.angle
                    )
            );
        }

        return rings;
    }
}
