package pictureanalysis.finder;

import org.opencv.core.*;
import pictureanalysis.features.Feature;

import java.util.List;

public interface Finder {
    List<Feature> find(Mat image);
}
