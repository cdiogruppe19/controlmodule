package pictureanalysis.supplier;

import java.awt.image.BufferedImage;

public class ImageInfo {

    private BufferedImage image;
    private String name;


    public ImageInfo(String name,BufferedImage image) {
        this.image = image;
        this.name = name;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
