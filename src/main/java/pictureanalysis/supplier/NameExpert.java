package pictureanalysis.supplier;


import java.util.HashSet;
import java.util.Set;

public class NameExpert {

    private static Set<String> names = new HashSet<>();

    public static void putName(String name){
        names.add(name);
    }

    public static Set<String> getNames(){
        return names;
    }
}
