package pictureanalysis.supplier;



import java.awt.image.BufferedImage;
import java.util.*;

public class ImageSupplier extends Observable {
    private static ImageSupplier currentSupplier=null;
    private Map<String, BufferedImage> images;
    private Map<Object,Map<String, BufferedImage>> temporaryImageStorage;
    private boolean initialized;

    public ImageSupplier() {
        images = new HashMap<>();
        temporaryImageStorage=new HashMap<>();
    }

    public static synchronized ImageSupplier getInstance(){
        if(currentSupplier==null){
            currentSupplier=new ImageSupplier();
        }
        return currentSupplier;
    }

    public void put(String name, BufferedImage image) {
        images.put(name, image);
        this.setChanged();
    }

    public void store(String name, BufferedImage image,Object context) {
        Map<String, BufferedImage> tempImages;
        if(!temporaryImageStorage.containsKey(context)){
            temporaryImageStorage.put(context,new HashMap<>());
        }
        tempImages = temporaryImageStorage.get(context);
        tempImages.put(name,image);
        this.setChanged();
    }

    public void commitStorage(Object context){

        Map<String, BufferedImage> committedImages =temporaryImageStorage.remove(context);
        if(committedImages==null){
            return;
        }
        for(String key : committedImages.keySet()){
            images.put(key,committedImages.get(key));
        }
        notifyObservers();
    }

    public BufferedImage getImage(String name) {
        return images.get(name);
    }

    public List<BufferedImage> getImages() {
        List<ImageInfo> imageInfos =  getImagesWithInfo();
        List<BufferedImage> outImages = new ArrayList<>();
        imageInfos.stream().forEach(info -> outImages.add(info.getImage()));
        return outImages;
    }

    public List<ImageInfo> getImagesWithInfo() {
        ArrayList<ImageInfo> imageInfos= new ArrayList<>();
        Set<String> names =  images.keySet();
        for(String name : names){
            BufferedImage image = images.get(name);
            ImageInfo imageInfo = new ImageInfo(name,image);
            imageInfos.add(imageInfo);
        }
        return imageInfos;
    }


    public List<String> getMapKeys() {
        return new ArrayList<>(images.keySet());
    }

    public void setInitialized(){
        if(initialized==false) {
            for (String name : NameExpert.getNames())
                put(name, null);
        }

        initialized=true;
    }

    public boolean areImageNamesInitialized(){
        return initialized;
    }
}