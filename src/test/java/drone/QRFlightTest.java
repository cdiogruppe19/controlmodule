package drone;

import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;
import de.yadrone.base.video.ImageListener;
import org.junit.Test;
import pictureanalysis.controller.AnalysisController;
import pictureanalysis.controller.AnalysisControllerBuilder;
import pictureanalysis.features.Feature;
import pictureanalysis.finder.qr.QRTextFinder;
import pictureanalysis.supplier.ImageSupplier;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class QRFlightTest {

    private IARDrone drone;
    private BufferedImage img;
    private AnalysisController ff;
    private List<Feature> features;
    //private static int imagenumber = 4;
    private int speed = 35;
    private static boolean allowedToMove = false;

    @Test
    public void doFlight() {

        //Initialize drone
        drone = new ARDrone();
        CommandManager cm = drone.getCommandManager();

        //Initialize featurefinder
        ff = new AnalysisControllerBuilder(null)
                .apply(new QRTextFinder())
                .drawFeaturesOnImages("QR")
                .build();


        //Create imagelistener
        drone.getVideoManager().addImageListener(new ImageListener() {
            @Override
            public void imageUpdated(BufferedImage bufferedImage) {
                img = bufferedImage;
                features = ff.findFeatures(img);
                move();
            }
        });

        drone.start();
        //cm.resetCommunicationWatchDog();


        cm.flatTrim();

        cm.takeOff().doFor(1000);
        cm.up(20).doFor(2000);
        cm.hover();

        allowedToMove = true;

        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        allowedToMove = false;

        cm.landing();

        drone.stop();


    }

    @Test
    public void flight2(){
        //Initialize drone
        drone = new ARDrone();
        CommandManager cm = drone.getCommandManager();
        ImageSupplier sup = new ImageSupplier();
        //Initialize featurefinder
        ff = new AnalysisControllerBuilder(sup)
                .apply(new QRTextFinder())
                .drawFeaturesOnImages("QR")
                .build();


        //Create imagelistener
        drone.getVideoManager().addImageListener(new ImageListener() {
            @Override
            public void imageUpdated(BufferedImage bufferedImage) {
                img = bufferedImage;
            }
        });

        drone.start();
        //cm.resetCommunicationWatchDog();


        cm.flatTrim();

        cm.takeOff().doFor(1000);
        cm.up(20).doFor(2000);
        cm.hover();

        while (img == null) {
            try {
                System.out.println("no img");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 100; i++) {
            System.out.println(img.getWidth() + " " + img.getHeight());

            features = ff.findFeatures(img);
            if (features.size() > 0) {
                printTheStuff(features.get(features.size() - 1));
                testMove();
            }
            features.clear();
        }


        allowedToMove = true;

        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        allowedToMove = false;

        cm.landing();

        drone.stop();
    }

    public void move() {

        CommandManager cm = drone.getCommandManager();

        int x_factor, y_factor, margin_x, margin_y, centerval_x, centerval_y;
        margin_x = 25;
        margin_y = 25;
        centerval_x = 320;
        centerval_y = 180;


        if (features.size() > 0) {
            for (Feature f : features) {
                System.out.println(f.toString());

                x_factor = direction(centerval_x, margin_x, f.getPos().x + (f.getWidth() / 2.0));
                y_factor = direction(centerval_y, margin_y, f.getPos().y + (f.getHeight() / 2.0));

                System.out.println(x_factor + " " + y_factor);

                if (x_factor == 0 && y_factor == 0 && allowedToMove) {
                    cm.move(speed, 0, 0, 0).doFor(250);
                } else if (allowedToMove) {
                    //System.out.println("MOVING!");
                    cm.move(0, -1 * speed * x_factor, speed * y_factor, 0);
                }
            }
        } else if (allowedToMove) {
            cm.hover();
            //cm.move(0,0,0,5);
        }


    }

    @Test
    public void testMove() {
        CommandManager cm = drone.getCommandManager();

        if (features.size() > 0) {
            Feature f = features.get(features.size() - 1);
            System.out.println(f.toString());

            if (f.getWidth() < 70 && f.getWidth() > 60) {
                if (f.getHeight() < 70 && f.getHeight() > 60) {
                    cm.up(speed).doFor(500);
                    cm.forward(speed).doFor(1000);
                    cm.down(speed).doFor(500);
                    cm.hover();
                }
            }
        } else {
            cm.hover();
        }
    }

    public int direction(double center, double margin, double position) {
        if (position < center - margin) {
            return -1;
        } else if (position > center + margin) {
            return 1;
        } else {
            return 0;
        }
    }

    @Test
    public void doPrint() {
        //Initialize drone
        drone = new ARDrone();
        CommandManager cm = drone.getCommandManager();
        ImageSupplier sup = new ImageSupplier();
        //Initialize featurefinder
        ff = new AnalysisControllerBuilder(sup)
                .apply(new QRTextFinder())
                .drawFeaturesOnImages("QR")
                .build();

        //Create imagelistener
        drone.getVideoManager().addImageListener(new ImageListener() {
            @Override
            public void imageUpdated(BufferedImage bufferedImage) {
                img = bufferedImage;
                /*
                System.out.println(img.getWidth() + " " + img.getHeight());
                features = ff.findFeatures(img);
                printTheStuff();
                try {
                    ImageIO.write(sup.getImage("QR"),"jpg", new File("test.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
            }
        });

        drone.start();
        //cm.resetCommunicationWatchDog();
        cm.landing();
        while (img == null) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 100; i++) {
            System.out.println(img.getWidth() + " " + img.getHeight());
            features = ff.findFeatures(img);
            if (features.size() > 0) {
                printTheStuff(features.get(features.size() - 1));
                try {
                    ImageIO.write(sup.getImage("QR"), "jpg", new File("test.jpg"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            features.clear();
        }
        /*
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*/
        drone.stop();
    }

    public void printTheStuff(Feature feature) {
        Feature f = feature;
        System.out.println(f.toString());
        /*
        for(Feature f : features){
            System.out.println(f.toString());
        }
        */
    }
}
