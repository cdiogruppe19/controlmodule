package drone;

import de.yadrone.base.video.ImageListener;
import drone_control.DroneController;
import drone_control.DroneNavigationImpl;
import main.gui.interfaces.DroneNavigationInterface;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DroneCameraTest {

    @Test
    public void testCamera() throws InterruptedException {
        DroneNavigationImpl dc = new DroneNavigationImpl();

        dc.getDrone().getVideoManager().addImageListener(new ImageListener() {
            int count = 0;

            @Override
            public void imageUpdated(BufferedImage bufferedImage) {
                System.out.println("THIS IS THE COUNT, MAAAN:" + count);
                if(count%20==0) {
                    System.out.println(bufferedImage.getWidth()+"x"+bufferedImage.getHeight());
                    File imageFile = new File("DroneImage/Image" + count +".jpg");
                    try {
                        ImageIO.write(bufferedImage, "jpg", imageFile);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                count++;
            }
        });

        dc.takeoff();
        Thread.sleep(12000);
        dc.land();

    }
}
