package drone;

import de.yadrone.base.ARDrone;
import de.yadrone.base.IARDrone;
import de.yadrone.base.command.CommandManager;
import drone_control.DroneNavigationImpl;
import drone_control.HoverTimer;
import org.junit.Test;

public class DroneNavigationImplTest {

    @Test
    public void flightTest() throws InterruptedException {
        DroneNavigationImpl dc = new DroneNavigationImpl();

        dc.takeoff();
        dc.auto();

        Thread.sleep(15000);

        dc.land();

    }

    @Test
    public void land(){
        IARDrone drone = new ARDrone();
        CommandManager cm = drone.getCommandManager();

        drone.start();
        cm.landing();
        drone.stop();

    }

    @Test
    public void timerTest() throws InterruptedException {
        DroneNavigationImpl dc = new DroneNavigationImpl();

        //HoverTimer.getInstance().resetTimer(200);

        Thread.sleep(1000);

    }
}
