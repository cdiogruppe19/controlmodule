package drone;

import drone_control.DroneNavigationImpl;
import main.gui.interfaces.DroneNavigationInterface;
import main.gui.interfaces.ImageProvider;
import org.junit.Test;
import pictureanalysis.ui.AnalysisGUI;

public class EllipseDroneTest {

    @Test
    public void testEllipseNav() throws InterruptedException {
        AnalysisGUI ag = new AnalysisGUI(new String[]{"Circle Features","Possible QR Features","2"});
        DroneNavigationImpl dni = new DroneNavigationImpl();

        dni.start();
        dni.takeoff();
        //dni.getDrone().getCommandManager().up(20);
        dni.auto();

        Thread.sleep(20000);


        dni.stop();
        dni.land();

        
    }
}
