package pictureanalysis.controller;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.junit.Assert;
import org.junit.Test;
import pictureanalysis.features.Feature;
import pictureanalysis.filter.image.GammaCorrectionImageFilter;
import pictureanalysis.filter.image.mask.WhiteMaskImageFilter;
import pictureanalysis.filter.image.smoothing.BlurImageFilter;
import pictureanalysis.finder.qr.QRFinder;
import pictureanalysis.finder.qr.QRTextFinder;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.ui.AnalysisGUI;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;


public class TestQRSquare {

    @Test
    public void testPicture() {
        File imageFile = new File("src/main/resources/photo/qr/goodQRImage.jpg");
        BufferedImage image;

        try {
            image = ImageIO.read(imageFile);
            if (image == null) {
                System.out.println("Image is null");
                return;
            }
            ImageSupplier imageSupp = ImageSupplier.getInstance();
            AnalysisController analysisController = new AnalysisControllerBuilder(imageSupp)
                    .apply(new GammaCorrectionImageFilter(4))
                    .saveLastImage("1")
                    .apply(new WhiteMaskImageFilter())
                    .apply(new BlurImageFilter(3))
                    .saveLastImage("2")
                    .apply(new QRFinder())
                    .drawFeaturesOnImages("original")
                    .build();
            analysisController.findFeatures(image);

            /*BufferedImage outImage=imageSupp.getImage("qrTest");
            File outImageFile = new File("testImage.jpg");
            ImageIO.write(outImage,"jpg",outImageFile);*/
            ImageIO.write(imageSupp.getImage("original"), "jpg", new File("target/original.jpg"));
            ImageIO.write(imageSupp.getImage("1"), "jpg", new File("target/1.jpg"));
            ImageIO.write(imageSupp.getImage("2"), "jpg", new File("target/2.jpg"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testVideo() throws Exception {
        ImageSupplier imageSupplier = ImageSupplier.getInstance();
        new AnalysisGUI(new String[]{"original", "1", "2"});
        QRFinder qrFinder = new QRFinder();
        //qrFinder.drawContours = true;

        System.out.println("Loading video");
        File f = new File("src/main/resources/video/videoQR.mp4");
        FFmpegFrameGrabber g = new FFmpegFrameGrabber(f);
        g.start();
        g.setFrameNumber(700);

        int nFrames = g.getLengthInFrames() - 1;

        while (g.getFrameNumber() < nFrames) {
            Frame frame = g.grab();
            BufferedImage image = new Java2DFrameConverter().convert(frame);
            imageSupplier.put("original", image);

            System.out.println("Frame " + g.getFrameNumber() + " of " + nFrames);

            AnalysisController ctrl = new AnalysisControllerBuilder(imageSupplier)
                    .apply(new GammaCorrectionImageFilter(1.5))
                    .saveLastImage("1")
                    .apply(new WhiteMaskImageFilter())
                    //.apply(new BlurImageFilter(3))
                    //.apply(new CannyEdgeImageFilter(10,100, 5))
                    //.apply(new DialateImageFilter(1))
                    .saveLastImage("2")
                    .apply(qrFinder)
                    .drawFeaturesOnImages("original")
                    .build();

            ctrl.findFeatures(image);
            //System.out.println(features);

/*
            BufferedImage outImage = imageSupplier.getImage("1");
            File outImageFile = new File("target/testImage.jpg");
            ImageIO.write(outImage, "jpg", outImageFile);
*/

            //Thread.sleep(1000);

        }
    }

    @Test
    public void testVideoComparison() throws Exception {
        SpeedTester speedTester = new SpeedTester();
        ImageSupplier imageSupplier = ImageSupplier.getInstance();
        List<Feature> featuresQRText, featuresQR;

        System.out.println("Loading video");
        File f = new File("src/main/resources/video/videoQR.mp4");
        FFmpegFrameGrabber g = new FFmpegFrameGrabber(f);
        g.start();
        g.setFrameNumber(1100);

        /** Frames in videoQR where feature found in QRTextFinder but not QRFinder:
         *  759
         * */

        int nFrames = g.getLengthInFrames() - 1;
        double speedQRText = 0, speedQR = 0;

        while (g.getFrameNumber() < nFrames) {
            Frame frame = g.grab();
            BufferedImage image = new Java2DFrameConverter().convert(frame);

            // OBS skipping frame that does not work in QRFinder
            if (g.getFrameNumber() == 759)
                continue;

            System.out.println("Frame " + g.getFrameNumber() + " of " + nFrames);

            speedTester.start();
            {
                AnalysisController ctrl = new AnalysisControllerBuilder(imageSupplier)
                        .apply(new GammaCorrectionImageFilter(1.5))
                        .apply(new QRTextFinder())
                        .build();

                featuresQRText = ctrl.findFeatures(image);
                System.out.println("QRText:\t" + featuresQRText);
            }
            speedQRText += speedTester.end();

            speedTester.start();
            {
                AnalysisController ctrl = new AnalysisControllerBuilder(imageSupplier)
                        .apply(new GammaCorrectionImageFilter(1.5))
                        .apply(new WhiteMaskImageFilter())
                        .apply(new QRFinder())
                        .build();

                featuresQR = ctrl.findFeatures(image);
                System.out.println("QR:\t\t" + featuresQR);
            }
            speedQR += speedTester.end();

            Assert.assertTrue(featuresQR.size() >= featuresQRText.size());

        }

        System.out.println("Total running time:" + "\nQRTextFinder:\t" + speedQRText + " ms\nQRFinder:\t\t" + speedQR + " ms");

        Assert.assertTrue(speedQR <= speedQRText);
    }
}
