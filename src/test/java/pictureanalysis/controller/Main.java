package pictureanalysis.controller;

import org.bytedeco.javacv.*;
import org.opencv.imgcodecs.Imgcodecs;
import pictureanalysis.filter.image.ImageFilter;
import pictureanalysis.filter.image.GammaCorrectionImageFilter;
import pictureanalysis.filter.image.smoothing.GaussianBlurImageFilter;
import pictureanalysis.filter.image.mask.RedMaskNormalImageFilter;
import pictureanalysis.finder.ring.SingleRingFinder;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.ui.AnalysisGUI;
import pictureanalysis.utility.ImageConverter;
import pictureanalysis.features.Feature;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Main{

    static BufferedImage myPicture;



    public static void main(String[] args) throws IOException, FrameGrabber.Exception {
        System.out.println("Started ring analyzer");
        SingleRingFinder ringFinder = new SingleRingFinder();
        System.out.println("Starting GUI");
        ImageSupplier imageSupplier = ImageSupplier.getInstance();
        AnalysisGUI ag =  new AnalysisGUI();

        System.out.println("Loading video");
        File f = new File("src/main/resources/video/video2.mp4");
        FFmpegFrameGrabber g = new FFmpegFrameGrabber(f);
        g.start();
        g.setFrameNumber(1300);


        int nFrames = g.getLengthInFrames();

        System.out.println("Starting show");
        outer : while(true){

            System.out.println("Frame "+ g.getFrameNumber() + " of " + nFrames);
            Frame frame = g.grab();
            myPicture= new Java2DFrameConverter().convert(frame);
            imageSupplier.put("original", myPicture);

            ImageFilter redMaskImageFilter = new RedMaskNormalImageFilter();
            ImageFilter gaussianBlur = new GaussianBlurImageFilter();
            ImageFilter gammaCorrection = new GammaCorrectionImageFilter(1.5);

            AnalysisController ctrl = new AnalysisControllerBuilder(imageSupplier)
                                .apply(gammaCorrection)
                                .saveLastImage("gamma")
                                .apply(redMaskImageFilter)
                                .saveLastImage("gammared")
                                .apply(gaussianBlur)
                                .saveLastImage("gammaredblur")
                                .apply(ringFinder)
                                .drawFeaturesOnImages("features")
                                .build();

            List<Feature> rings = ctrl.findFeatures(myPicture);

            Imgcodecs.imwrite("target/gamma.png", ImageConverter.bufferedImageToMatRGB(imageSupplier.getImage("gamma")));
            Imgcodecs.imwrite("target/gammared.png", ImageConverter.bufferedImageToMatBW(imageSupplier.getImage("gammared")));
            Imgcodecs.imwrite("target/gammeredblur.png", ImageConverter.bufferedImageToMatBW(imageSupplier.getImage("gammaredblur")));

            for(Feature ring : rings){
                if(ring.getWidth()>300&&ring.getHeight()>300){
                    //break outer;
                }
            }
            ag.update();

        }
        //ag.update();




    }
}
