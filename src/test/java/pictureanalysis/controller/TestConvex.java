package pictureanalysis.controller;

import pictureanalysis.supplier.ImageSupplier;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;


public class TestConvex {

    public static void main(String[] args){

        File imageFile= new File("src/main/resources/photo/ring/sidering_small.jpg");
        //File imageFile= new File("src/main/resources/photo/qr/DroneImage60.jpg");
        BufferedImage image;

        try {
            image = ImageIO.read(imageFile);
            if(image==null){
                System.out.println("Image is null");
                return;
            }
            ImageSupplier imageSupp = ImageSupplier.getInstance();


            System.out.println("Warming up");
            for(int i=0;i<10;i++) {
                AnalysisLibrary.findAllFeatures(image);
            }

            int n=15;
            long totalTime = 0;
            System.out.println("Measuring average run time.");
            for(int i=0;i<n;i++) {
                long start = System.currentTimeMillis();
                AnalysisLibrary.findAllFeatures(image);
                long endtime =System.currentTimeMillis()-start;
                totalTime += endtime;
                System.out.println(endtime);
            }
            System.out.println("Average time: "+ totalTime/n);


            List<String> imageNames = imageSupp.getMapKeys();
            for(String name: imageNames) {
                BufferedImage outImage = imageSupp.getImage(name);
                File outImageFile = new File("target/testImages/"+name.replaceAll(" ","")+".jpg");
                outImageFile.mkdirs();
                ImageIO.write(outImage, "jpg", outImageFile);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
