package pictureanalysis.controller;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.SynchronousQueue;

public class SpeedTester {
    private long start=0;
    private int numOfTimesToAverage=10;
    private ArrayList<Double> times;

    public SpeedTester(){
        times= new ArrayList<>();
    }

    public void start(){
        start= System.currentTimeMillis();
    }

    public double end(){
        double time = (double)(System.currentTimeMillis()-start);
        System.out.printf("Analysis Time: %.2fms(%.2f fps)\n",time, 1000.0/time);
        if(times.size()>numOfTimesToAverage){
            times.remove(0);
        }
        times.add(time);

        double sum=0;
        for(double tempTime : times){
            sum+=tempTime;
        }
        System.out.println("Current Average: "+sum/times.size());
        return time;
    }
}
