package pictureanalysis.controller;

import pictureanalysis.finder.qr.QRTextFinder;
import pictureanalysis.supplier.ImageSupplier;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class TestQR {

    public static void main(String[] args){
        File imageFile= new File("src/main/resources/photo/qr/goodQRImage.jpg");
        BufferedImage image;

        try {
            image = ImageIO.read(imageFile);
            if(image==null){
                System.out.println("Image is null");
                return;
            }
            ImageSupplier imageSupp = ImageSupplier.getInstance();
            AnalysisController analysisController =new AnalysisControllerBuilder(imageSupp)
                    .apply(new QRTextFinder())
                    .drawFeaturesOnImages("QR")
                    .build();
            analysisController.findFeatures(image);
            BufferedImage outImage=imageSupp.getImage("QR");
            File outImageFile = new File("target/testImage.jpg");

            ImageIO.write(outImage,"jpg",outImageFile);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
