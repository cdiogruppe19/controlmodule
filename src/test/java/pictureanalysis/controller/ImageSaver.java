package pictureanalysis.controller;

import pictureanalysis.supplier.ImageSupplier;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class ImageSaver {


    public static void saveImages(List<String> imageNames){
        for(String name: imageNames) {
            BufferedImage outImage = ImageSupplier.getInstance().getImage(name);
            File outImageFile = new File("target/testImages/"+name.replaceAll(" ","")+".jpg");
            outImageFile.mkdirs();
            try {
                ImageIO.write(outImage, "jpg", outImageFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
