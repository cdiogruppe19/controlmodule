package pictureanalysis.controller;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.junit.Test;
import pictureanalysis.filter.feature.EllipseFilter;
import pictureanalysis.filter.image.GammaCorrectionImageFilter;
import pictureanalysis.filter.image.blob.BlobRemover;
import pictureanalysis.filter.image.mask.BinaryThresholdImageFilter;
import pictureanalysis.filter.image.mask.RedMaskDarkImageFilter;
import pictureanalysis.filter.image.smoothing.GaussianBlurImageFilter;
import pictureanalysis.finder.ring.ConvexRingFinder;
import pictureanalysis.finder.ring.SingleRingFinder;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.ui.AnalysisGUI;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;


public class TestDarkFilter {

    @Test
    public void testImage(){

        File imageFile= new File("src/main/resources/photo/ring/BadLight.png");
        //File imageFile= new File("src/main/resources/photo/qr/DroneImage60.jpg");
        BufferedImage image;

        try {
            image = ImageIO.read(imageFile);
            if(image==null){
                System.out.println("Image is null");
                return;
            }
            ImageSupplier imageSupp = ImageSupplier.getInstance();


            new AnalysisControllerBuilder()
                    .apply(new GammaCorrectionImageFilter(2))
                    .saveLastImage("Gamma Corrected")
                    .apply(new RedMaskDarkImageFilter())
                    .apply(new GaussianBlurImageFilter())
                    .saveLastImage("Dark Mask")
                    .apply(new BinaryThresholdImageFilter(100))
                    .saveLastImage("Binary Threshold")
                    .apply(new SingleRingFinder())
                    .apply(new BlobRemover())
                    .saveLastImage("Blobs Removed")
                    .apply(new ConvexRingFinder())
                    .apply(new EllipseFilter())
                    .drawFeaturesOnImages("Circle Features")
                    .build()
                    .findFeatures(image);


            List<String> imageNames = imageSupp.getMapKeys();
            ImageSaver.saveImages(imageNames);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void testVideo() throws Exception {
        ImageSupplier imageSupplier = ImageSupplier.getInstance();
        new AnalysisGUI(new String[]{"original", "Gamma Corrected", "Dark Mask"});

        System.out.println("Loading video");
        File f = new File("src/main/resources/video/video2.mp4");
        FFmpegFrameGrabber g = new FFmpegFrameGrabber(f);
        g.start();
        g.setFrameNumber(1100);

        int nFrames = g.getLengthInFrames() - 1;

        while (g.getFrameNumber() < nFrames) {
            Frame frame = g.grab();
            System.out.println("Frame " + g.getFrameNumber() + " of " + nFrames);

            BufferedImage image = new Java2DFrameConverter().convert(frame);
            imageSupplier.put("original", image);

            new AnalysisControllerBuilder(imageSupplier)
                    .apply(new GammaCorrectionImageFilter(1.5))
                    .saveLastImage("Gamma Corrected")
                    .apply(new RedMaskDarkImageFilter())
                    .apply(new GaussianBlurImageFilter())
                    .saveLastImage("Dark Mask")
                    .apply(new BinaryThresholdImageFilter(100))
                    .saveLastImage("Binary Threshold")
                    .apply(new SingleRingFinder())
                    .apply(new BlobRemover())
                    .saveLastImage("Blobs Removed")
                    .apply(new ConvexRingFinder())
                    .apply(new EllipseFilter())
                    .drawFeaturesOnImages("Circle Features")
                    .build()
                    .findFeatures(image);

            Thread.sleep(100);

            ImageIO.write(imageSupplier.getImage("Gamma Corrected"), "png", new File("target/gamma.png"));
            ImageIO.write(imageSupplier.getImage("Dark Mask"), "png", new File("target/mask.png"));

            Thread.sleep(100);

        }
    }

}
