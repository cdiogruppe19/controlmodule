package pictureanalysis.controller;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.Java2DFrameConverter;

import pictureanalysis.filter.image.GammaCorrectionImageFilter;
import pictureanalysis.finder.ring.SingleRingFinder;
import pictureanalysis.supplier.ImageSupplier;
import pictureanalysis.ui.AnalysisGUI;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class VideoFeedAllFeaturesTest {
    static BufferedImage myPicture;



    public static void main(String[] args) throws IOException{
        System.out.println("Starting GUI");
        ImageSupplier imageSupplier = ImageSupplier.getInstance();
        String[] imageNames = new String[]{"1","Possible QR Features","2"};
        AnalysisGUI ag =  new AnalysisGUI(imageNames);

        System.out.println("Loading video");
        File f = new File("src/main/resources/video/videoQR.mp4");
        FFmpegFrameGrabber g = new FFmpegFrameGrabber(f);
        g.start();
        //1300 good start for demonstration
        //950 against window
        g.setFrameNumber(100);


        int nFrames = g.getLengthInFrames();
        SpeedTester speedTest= new SpeedTester();
        AnalysisController ac = new AnalysisControllerBuilder()
                .apply(new GammaCorrectionImageFilter())
                .build();


        System.out.println("Starting show");
        outer : while(true){

            System.out.println("Frame "+ g.getFrameNumber() + " of " + nFrames);
            Frame frame = g.grab();
            myPicture= new Java2DFrameConverter().convert(frame);
            imageSupplier.put("original", myPicture);
            //speedTest.start();
            AnalysisLibrary.findAllFeatures(myPicture);
            //ac.findFeatures(myPicture);
            //speedTest.end();
            System.out.println();
        }
    }
}
